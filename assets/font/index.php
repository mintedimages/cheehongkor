<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>SIRALAI</title>
        <meta name="author" content="MINTEDIMAGES CO.,LTD.">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <style type="text/css" MEDIA="screen, print">


            @font-face {
                font-family: 'trajan_proregular';
                src: url('trajanpro-regular-webfont.eot');
                src: url('trajanpro-regular-webfont.eot?#iefix') format('embedded-opentype'),
                    url('trajanpro-regular-webfont.woff') format('woff'),
                    url('trajanpro-regular-webfont.ttf') format('truetype'),
                    url('trajanpro-regular-webfont.svg#trajan_proregular') format('svg');
                font-weight: normal;
                font-style: normal;

            }
            @font-face {
                font-family: 'HelveThaiCa2';
                src: url('DB_helvethaica_X.eot');
                src: url('DB_helvethaica_X.eot?#iefix') format('embedded-opentype'),
                    url('db_helvethaica_x.woff') format('woff'),
                    url('DB_helvethaica_X.ttf') format('truetype'),
                    url('db_helvethaica_x.svg#db_helvethaica_x') format('svg');
                font-weight: normal;
                font-style: normal;

            }
            @font-face {
                font-family: HelveThaiCa;
                src: url(DB_helvethaica_X.eot);                     /* for use with older user agents */
                src: local("HelveThaiCa"), url(db_helvethaica_x.woff);  /* Overrides src definition */
            }
            body {

                color:#000;
                font-size:62.5%;

            }h1 { 
                font-family: 'trajan_proregular';
                font-size: 24px;      /* กรณีไม่รองรับ rem */
                font-size: 2.4rem;    /* กรณีรองรับ rem */
            }
            p { 
                font-family: 'HelveThaiCa';
                font-size: 24px;      /* กรณีไม่รองรับ rem */
                font-size: 2.4rem;    /* กรณีรองรับ rem */
            }
            h2 { 
                font-family: 'THSarabunPSKRegular';
                font-size: 24px;      /* กรณีไม่รองรับ rem */
                font-size: 2.4rem;    /* กรณีรองรับ rem */
                letter-spacing: 0;


            </style>
        </head>
        <body>
            <h1>font-family: 'TrajanProRegular'</h1>
            <p>   
                จาก workshop ข้างต้น จะเห็นว่าเราได้สร้าง html element ขึ้นมาเพื่อสร้างสามเหลี่ยมโดยเฉพาะ ซึ่งจริงๆ แล้ว รูปสามเหลี่ยมนี้ไม่ถือว่าเป็น content เราจึงไม่ควรสร้างมันโดยใช้ html ให้เราเปลี่ยนมาใช้ css pseudo-element แทน สมมติเราจะสร้าง tooltip ขึ้นมาสักอันหนึ่ง โค้ด html เราเป็นแบบนี้
            </p>
            <h2>     จาก workshop ข้างต้น จะเห็นว่าเราได้สร้าง html element ขึ้นมาเพื่อสร้างสามเหลี่ยมโดยเฉพาะ ซึ่งจริงๆ แล้ว รูปสามเหลี่ยมนี้ไม่ถือว่าเป็น content เราจึงไม่ควรสร้างมันโดยใช้ html ให้เราเปลี่ยนมาใช้ css pseudo-element แทน สมมติเราจะสร้าง tooltip ขึ้นมาสักอันหนึ่ง โค้ด html เราเป็นแบบนี้
            </h2>
            <h3 class="DBAdmanXRegularSVG">
                111จาก workshop ข้างต้น จะเห็นว่าเราได้สร้าง html element ขึ้นมาเพื่อสร้างสามเหลี่ยมโดยเฉพาะ ซึ่งจริงๆ แล้ว รูปสามเหลี่ยมนี้ไม่ถือว่าเป็น content เราจึงไม่ควรสร้างมันโดยใช้ html ให้เราเปลี่ยนมาใช้ css pseudo-element แทน สมมติเราจะสร้าง tooltip ขึ้นมาสักอันหนึ่ง โค้ด html เราเป็นแบบนี้


            </h3>
        </body>
    </html>
