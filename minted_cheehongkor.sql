-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2014 at 03:06 PM
-- Server version: 5.0.96-community
-- PHP Version: 5.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `minted_cheehongkor`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_query`
--

CREATE TABLE IF NOT EXISTS `ci_query` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `query_string` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ci_query`
--

INSERT INTO `ci_query` (`id`, `query_string`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `log_action`
--

CREATE TABLE IF NOT EXISTS `log_action` (
  `action_id` bigint(20) unsigned NOT NULL auto_increment,
  `table_name` char(30) NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `action_name` varchar(255) NOT NULL,
  `action_detail` text NOT NULL,
  `action_query` text NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_ip` varchar(50) NOT NULL,
  PRIMARY KEY  (`action_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=401 ;

--
-- Dumping data for table `log_action`
--

INSERT INTO `log_action` (`action_id`, `table_name`, `content_id`, `action_name`, `action_detail`, `action_query`, `create_by`, `create_date`, `create_ip`) VALUES
(1, 'mother_table', 1, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Register<br />table_code : register<br />table_type : dynamic<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Register'', ''register'', ''dynamic'', ''1'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-21 15:50:52', '::1'),
(2, 'mother_column', 1, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : register_name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />sort_priority : 1<br />table_id : 1', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''register_name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', ''1'', ''1'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-21 15:51:54', '::1'),
(3, 'mother_column', 1, 'delete column', 'model : StructureModel<br />column_id : 1<br />column_main : false => true', 'UPDATE `mother_column` SET `column_main` = ''true'', `update_date` = NOW(), `update_by` = ''1'' WHERE `column_id` =  ''1''', 1, '2013-11-21 15:52:07', '::1'),
(4, 'mother_column', 2, 'create column', 'model : StructureModel<br />column_name : Last Name<br />column_code : register_last_name<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />sort_priority : 2<br />table_id : 1', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Last Name'', ''register_last_name'', ''false'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', ''2'', ''1'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-21 15:52:23', '::1'),
(5, 'mother_column', 2, 'delete column', 'model : StructureModel<br />column_id : 2', 'DELETE FROM `mother_column`\nWHERE `column_id` =  ''2''', 1, '2013-11-21 15:52:45', '::1'),
(6, 'mother_table', 1, 'delete table', 'model : StructureModel<br />table_id : 1', 'DELETE FROM `mother_table`\nWHERE `table_id` =  ''1''', 1, '2013-11-21 15:59:41', '::1'),
(7, 'mother_table', 2, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Register<br />table_code : register<br />table_type : dynamic<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Register'', ''register'', ''dynamic'', ''1'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-21 16:01:06', '::1'),
(8, 'mother_column', 3, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : register_name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />sort_priority : 1<br />table_id : 2', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''register_name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', ''1'', ''2'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-21 16:01:36', '::1'),
(9, 'tbl_register', 1, 'insert Register', 'model : BackendModel<br />register_name : x<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''x'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-21 16:02:24', '::1'),
(10, 'tbl_register_lang', 1, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''1'', ''1'')', 1, '2013-11-21 16:02:24', '::1'),
(11, 'tbl_register', 2, 'insert Register', 'model : BackendModel<br />register_name : y<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''y'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-21 16:02:55', '::1'),
(12, 'tbl_register_lang', 2, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''2'', ''1'')', 1, '2013-11-21 16:02:55', '::1'),
(13, 'tbl_register', 1, 'delete Register', 'model : BackendModel<br />register_id : 1', 'UPDATE `tbl_register` SET `enable_status` = ''delete'' WHERE `register_id` =  ''1''', 1, '2013-11-25 13:04:55', '::1'),
(14, 'tbl_register', 3, 'insert Register', 'model : BackendModel<br />register_name : z<br />sort_priority : 2<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''z'', ''2'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-25 13:05:04', '::1'),
(15, 'tbl_register_lang', 3, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''3'', ''1'')', 1, '2013-11-25 13:05:04', '::1'),
(16, 'tbl_register', 3, 'delete Register', 'model : BackendModel<br />register_id : 3', 'UPDATE `tbl_register` SET `enable_status` = ''delete'' WHERE `register_id` =  ''3''', 1, '2013-11-25 13:06:58', '::1'),
(17, 'tbl_register', 4, 'insert Register', 'model : BackendModel<br />register_name : x<br />sort_priority : 2<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''x'', ''2'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-25 13:38:51', '::1'),
(18, 'tbl_register_lang', 4, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''4'', ''1'')', 1, '2013-11-25 13:38:51', '::1'),
(19, 'tbl_register', 5, 'insert Register', 'model : BackendModel<br />register_name : z<br />sort_priority : 3<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''z'', ''3'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-25 13:38:59', '::1'),
(20, 'tbl_register_lang', 5, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''5'', ''1'')', 1, '2013-11-25 13:38:59', '::1'),
(21, 'tbl_register', 6, 'insert Register', 'model : BackendModel<br />register_name : a<br />sort_priority : 4<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''a'', ''4'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-25 13:39:08', '::1'),
(22, 'tbl_register_lang', 6, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''6'', ''1'')', 1, '2013-11-25 13:39:08', '::1'),
(23, 'tbl_register', 7, 'insert Register', 'model : BackendModel<br />register_name : b<br />sort_priority : 5<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''b'', ''5'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-25 13:39:18', '::1'),
(24, 'tbl_register_lang', 7, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''7'', ''1'')', 1, '2013-11-25 13:39:18', '::1'),
(25, 'tbl_register', 6, 'delete Register', 'model : BackendModel<br />register_id : 6', 'UPDATE `tbl_register` SET `enable_status` = ''delete'' WHERE `register_id` =  ''6''', 1, '2013-11-25 14:02:19', '::1'),
(26, 'mother_table', 3, 'create table', 'model : StructureModel<br />parent_table_id : 2<br />table_name : Sub Content<br />table_code : sub_content<br />table_type : dynamic<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''2'', ''Sub Content'', ''sub_content'', ''dynamic'', ''1'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-27 09:32:13', '::1'),
(27, 'mother_table', 3, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''3''', 1, '2013-11-27 09:32:36', '::1'),
(28, 'mother_table', 3, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''3''', 1, '2013-11-27 09:35:04', '::1'),
(29, 'mother_table', 3, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''3''', 1, '2013-11-27 09:38:13', '::1'),
(30, 'mother_table', 3, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''3''', 1, '2013-11-27 09:39:20', '::1'),
(31, 'mother_table', 2, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''2''', 1, '2013-11-27 09:39:35', '::1'),
(32, 'mother_table', 2, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''2''', 1, '2013-11-27 09:41:09', '::1'),
(33, 'mother_table', 2, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''2''', 1, '2013-11-27 09:41:17', '::1'),
(34, 'mother_table', 3, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''3''', 1, '2013-11-27 09:41:24', '::1'),
(35, 'mother_table', 3, 'update table', 'model : StructureModel<br /><b>Update</b>', 'UPDATE `mother_table` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `table_id` =  ''3''', 1, '2013-11-27 09:43:57', '::1'),
(36, 'mother_column', 4, 'create column', 'model : StructureModel<br />column_name : a<br />column_code : a<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />sort_priority : 1<br />table_id : 3', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''a'', ''a'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', ''1'', ''3'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-27 09:44:19', '::1'),
(37, 'tbl_sub_content', 1, 'insert Sub Content', 'model : BackendModel<br />parent_id : 2<br />a : a<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''2'', ''a'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-27 09:44:30', '::1'),
(38, 'tbl_sub_content_lang', 1, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''1'', ''1'')', 1, '2013-11-27 09:44:30', '::1'),
(39, 'tbl_sub_content', 2, 'insert Sub Content', 'model : BackendModel<br />parent_id : 2<br />a : b<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''2'', ''b'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-27 09:44:51', '::1'),
(40, 'tbl_sub_content_lang', 2, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''2'', ''1'')', 1, '2013-11-27 09:44:51', '::1'),
(41, 'tbl_sub_content', 3, 'insert Sub Content', 'model : BackendModel<br />parent_id : 2<br />a : c<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''2'', ''c'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-27 09:44:58', '::1'),
(42, 'tbl_sub_content_lang', 3, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''3'', ''1'')', 1, '2013-11-27 09:44:58', '::1'),
(43, 'tbl_sub_content', 3, 'update Sub Content', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 1 => 3', 'UPDATE `tbl_sub_content` SET `sort_priority` = ''3'', `update_date` = NOW(), `update_by` = ''1'' WHERE `sub_content_id` =  ''3''', 1, '2013-11-27 09:45:01', '::1'),
(44, 'tbl_sub_content_lang', 3, 'insert Sub Content_lang', 'model : BackendModel', 'UPDATE `tbl_sub_content_lang` SET `sub_content_id` = ''3'', `lang_id` = ''1'' WHERE `sub_content_lang_id` =  ''3''', 1, '2013-11-27 09:45:01', '::1'),
(45, 'tbl_sub_content', 2, 'update Sub Content', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 1 => 2', 'UPDATE `tbl_sub_content` SET `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''1'' WHERE `sub_content_id` =  ''2''', 1, '2013-11-27 09:45:07', '::1'),
(46, 'tbl_sub_content_lang', 2, 'insert Sub Content_lang', 'model : BackendModel', 'UPDATE `tbl_sub_content_lang` SET `sub_content_id` = ''2'', `lang_id` = ''1'' WHERE `sub_content_lang_id` =  ''2''', 1, '2013-11-27 09:45:07', '::1'),
(47, 'mother_shop', 2, 'create shop', 'model : ShopModel<br />shop_name : Secornd<br />shop_code : Secornd<br />sort_priority : 2', 'INSERT INTO `mother_shop` (`shop_name`, `shop_code`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Secornd'', ''Secornd'', ''2'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-27 09:49:18', '::1'),
(48, 'mother_lang', 2, 'create lang', 'model : LangModel<br />lang_name : English<br />lang_code : english<br />sort_priority : 2', 'INSERT INTO `mother_lang` (`lang_name`, `lang_code`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''English'', ''english'', ''2'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-27 09:49:48', '::1'),
(49, 'mother_config_group', 2, 'create config group0', 'model : ConfigGroupModel<br />config_group_name : Secornd Config<br />sort_priority : 2', 'INSERT INTO `mother_config_group` (`config_group_name`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Secornd Config'', ''2'', NOW(), ''1'', NOW(), ''1'')', 1, '2013-11-27 09:50:35', '::1'),
(50, 'tbl_register', 2, 'update Register', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 1 => 1', 'UPDATE `tbl_register` SET `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''1'' WHERE `register_id` =  ''2''', 1, '2013-11-27 09:55:41', '::1'),
(51, 'tbl_register_lang', 2, 'insert Register_lang', 'model : BackendModel', 'UPDATE `tbl_register_lang` SET `register_id` = ''2'', `lang_id` = ''1'' WHERE `register_lang_id` =  ''2''', 1, '2013-11-27 09:55:41', '::1'),
(52, 'tbl_register_lang', 7, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''2'', ''2'')', 1, '2013-11-27 09:55:41', '::1'),
(53, 'tbl_sub_content', 4, 'insert Sub Content', 'model : BackendModel<br />parent_id : 4<br />a : x<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''4'', ''x'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-27 10:02:07', '::1'),
(54, 'tbl_sub_content_lang', 4, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''4'', ''1'')', 1, '2013-11-27 10:02:07', '::1'),
(55, 'tbl_sub_content_lang', 4, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''4'', ''2'')', 1, '2013-11-27 10:02:07', '::1'),
(56, 'tbl_sub_content', 5, 'insert Sub Content', 'model : BackendModel<br />parent_id : 4<br />a : y<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''4'', ''y'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-27 10:02:13', '::1'),
(57, 'tbl_sub_content_lang', 5, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''5'', ''1'')', 1, '2013-11-27 10:02:13', '::1'),
(58, 'tbl_sub_content_lang', 5, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''5'', ''2'')', 1, '2013-11-27 10:02:13', '::1'),
(59, 'tbl_sub_content', 5, 'update Sub Content', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 1 => 2', 'UPDATE `tbl_sub_content` SET `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''1'' WHERE `sub_content_id` =  ''5''', 1, '2013-11-27 10:02:16', '::1'),
(60, 'tbl_sub_content_lang', 6, 'insert Sub Content_lang', 'model : BackendModel', 'UPDATE `tbl_sub_content_lang` SET `sub_content_id` = ''5'', `lang_id` = ''1'' WHERE `sub_content_lang_id` =  ''6''', 1, '2013-11-27 10:02:16', '::1'),
(61, 'tbl_sub_content_lang', 7, 'insert Sub Content_lang', 'model : BackendModel', 'UPDATE `tbl_sub_content_lang` SET `sub_content_id` = ''5'', `lang_id` = ''2'' WHERE `sub_content_lang_id` =  ''7''', 1, '2013-11-27 10:02:16', '::1'),
(62, 'tbl_sub_content', 6, 'insert Sub Content', 'model : BackendModel<br />parent_id : 4<br />a : z<br />sort_priority : 3<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''4'', ''z'', ''3'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2013-11-27 10:02:23', '::1'),
(63, 'tbl_sub_content_lang', 6, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''6'', ''1'')', 1, '2013-11-27 10:02:23', '::1'),
(64, 'tbl_sub_content_lang', 6, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''6'', ''2'')', 1, '2013-11-27 10:02:24', '::1'),
(65, 'tbl_register', 8, 'insert Register', 'model : BackendModel<br />register_name : a<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''a'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:11:34', '::1'),
(66, 'tbl_register_lang', 8, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''8'', ''1'')', 1, '2013-11-27 10:11:34', '::1'),
(67, 'tbl_register_lang', 8, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''8'', ''2'')', 1, '2013-11-27 10:11:34', '::1'),
(68, 'tbl_register', 9, 'insert Register', 'model : BackendModel<br />register_name : b<br />sort_priority : 2<br />enable_status : show', 'INSERT INTO `tbl_register` (`register_name`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''b'', ''2'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:11:45', '::1'),
(69, 'tbl_register_lang', 9, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''9'', ''1'')', 1, '2013-11-27 10:11:45', '::1'),
(70, 'tbl_register_lang', 9, 'insert Register_lang', 'model : BackendModel', 'INSERT INTO `tbl_register_lang` (`register_id`, `lang_id`) VALUES (''9'', ''2'')', 1, '2013-11-27 10:11:45', '::1'),
(71, 'tbl_sub_content', 7, 'insert Sub Content', 'model : BackendModel<br />parent_id : 8<br />a : x<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''8'', ''x'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:11:54', '::1'),
(72, 'tbl_sub_content_lang', 7, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''7'', ''1'')', 1, '2013-11-27 10:11:54', '::1'),
(73, 'tbl_sub_content_lang', 7, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''7'', ''2'')', 1, '2013-11-27 10:11:54', '::1'),
(74, 'tbl_sub_content', 8, 'insert Sub Content', 'model : BackendModel<br />parent_id : 8<br />a : y<br />sort_priority : 2<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''8'', ''y'', ''2'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:12:01', '::1'),
(75, 'tbl_sub_content_lang', 8, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''8'', ''1'')', 1, '2013-11-27 10:12:01', '::1'),
(76, 'tbl_sub_content_lang', 8, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''8'', ''2'')', 1, '2013-11-27 10:12:02', '::1'),
(77, 'tbl_sub_content', 9, 'insert Sub Content', 'model : BackendModel<br />parent_id : 8<br />a : z<br />sort_priority : 3<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''8'', ''z'', ''3'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:12:07', '::1'),
(78, 'tbl_sub_content_lang', 9, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''9'', ''1'')', 1, '2013-11-27 10:12:07', '::1'),
(79, 'tbl_sub_content_lang', 9, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''9'', ''2'')', 1, '2013-11-27 10:12:07', '::1'),
(80, 'tbl_sub_content', 10, 'insert Sub Content', 'model : BackendModel<br />parent_id : 9<br />a : h<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''9'', ''h'', ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:13:42', '::1'),
(81, 'tbl_sub_content_lang', 10, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''10'', ''1'')', 1, '2013-11-27 10:13:43', '::1'),
(82, 'tbl_sub_content_lang', 10, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''10'', ''2'')', 1, '2013-11-27 10:13:43', '::1'),
(83, 'tbl_sub_content', 11, 'insert Sub Content', 'model : BackendModel<br />parent_id : 9<br />a : j<br />sort_priority : 2<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''9'', ''j'', ''2'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:13:49', '::1'),
(84, 'tbl_sub_content_lang', 11, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''11'', ''1'')', 1, '2013-11-27 10:13:49', '::1'),
(85, 'tbl_sub_content_lang', 11, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''11'', ''2'')', 1, '2013-11-27 10:13:49', '::1'),
(86, 'tbl_sub_content', 12, 'insert Sub Content', 'model : BackendModel<br />parent_id : 9<br />a : k<br />sort_priority : 3<br />enable_status : show', 'INSERT INTO `tbl_sub_content` (`parent_id`, `a`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''9'', ''k'', ''3'', ''show'', NOW(), ''1'', NOW(), ''1'', ''2'')', 1, '2013-11-27 10:13:55', '::1'),
(87, 'tbl_sub_content_lang', 12, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''12'', ''1'')', 1, '2013-11-27 10:13:56', '::1'),
(88, 'tbl_sub_content_lang', 12, 'insert Sub Content_lang', 'model : BackendModel', 'INSERT INTO `tbl_sub_content_lang` (`sub_content_id`, `lang_id`) VALUES (''12'', ''2'')', 1, '2013-11-27 10:13:56', '::1'),
(89, 'mother_user', 2, 'create user', 'model : UserModel<br />user_group_id : 0<br />user_login_name : admin<br />enable_status : enable<br />user_login_password : 21232f297a57a5a743894a0e4a801fc3', 'INSERT INTO `mother_user` (`user_group_id`, `user_login_name`, `enable_status`, `user_login_password`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''admin'', ''enable'', ''21232f297a57a5a743894a0e4a801fc3'', NOW(), ''1'', NOW(), ''1'')', 1, '2014-06-03 17:27:09', '127.0.0.1'),
(90, 'mother_user', 2, 'update User', 'model : UserModel<br /><b>Update</b>', 'UPDATE `mother_user` SET `update_date` = NOW(), `update_by` = ''1'' WHERE `user_id` =  ''2''', 1, '2014-06-03 17:27:12', '127.0.0.1'),
(91, 'mother_table', 4, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Home Slide<br />table_code : home_slide<br />table_type : dynamic<br />table_order : asc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Home Slide'', ''home_slide'', ''dynamic'', ''asc'', ''true'', ''false'', '''', ''1'', ''1'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-06 14:46:23', '127.0.0.1'),
(92, 'mother_table', 2, 'delete table', 'model : StructureModel<br />table_id : 2', 'DELETE FROM `mother_table`\nWHERE `table_id` =  ''2''', 2, '2014-06-06 14:47:25', '127.0.0.1'),
(93, 'mother_column', 5, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 4', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''image'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''4'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-06 14:57:12', '127.0.0.1'),
(94, 'mother_column', 5, 'delete column', 'model : StructureModel<br />column_id : 5<br />column_main : false => true<br />column_field_type : image => text', 'UPDATE `mother_column` SET `column_main` = ''true'', `column_field_type` = ''text'', `update_date` = NOW(), `update_by` = ''2'' WHERE `column_id` =  ''5''', 2, '2014-06-06 14:57:34', '127.0.0.1'),
(95, 'mother_column', 6, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 4', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''4'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-06 14:57:51', '127.0.0.1'),
(96, 'mother_table', 5, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : About us<br />table_code : about<br />table_type : dynamic<br />table_order : asc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 2', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''About us'', ''about'', ''dynamic'', ''asc'', ''true'', ''false'', '''', ''1'', ''2'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:24:51', '127.0.0.1'),
(97, 'mother_column', 7, 'create column', 'model : StructureModel<br />column_name : Title<br />column_code : title<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 5', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Title'', ''title'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''5'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:25:54', '127.0.0.1'),
(98, 'mother_column', 8, 'create column', 'model : StructureModel<br />column_name : Date<br />column_code : date<br />column_main : false<br />column_field_type : date<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 5', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Date'', ''date'', ''false'', ''date'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''5'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:26:22', '127.0.0.1'),
(99, 'mother_column', 9, 'create column', 'model : StructureModel<br />column_name : Detail<br />column_code : detail<br />column_main : false<br />column_field_type : richtext<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 5', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Detail'', ''detail'', ''false'', ''richtext'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''3'', ''5'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:27:32', '127.0.0.1'),
(100, 'mother_column', 10, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 4<br />table_id : 5', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''4'', ''5'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:27:48', '127.0.0.1'),
(101, 'mother_table', 6, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Inside<br />table_code : inside<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 3', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Inside'', ''inside'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''3'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:29:26', '127.0.0.1'),
(102, 'mother_column', 11, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 6', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''6'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:29:42', '127.0.0.1'),
(103, 'mother_column', 12, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 6', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''6'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:29:56', '127.0.0.1'),
(104, 'mother_table', 7, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Article<br />table_code : article<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Article'', ''article'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''1'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:32:02', '127.0.0.1'),
(105, 'mother_column', 13, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 7', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''7'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:32:27', '127.0.0.1'),
(106, 'mother_column', 14, 'create column', 'model : StructureModel<br />column_name : Title<br />column_code : title<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 7', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Title'', ''title'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''7'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:32:42', '127.0.0.1'),
(107, 'mother_column', 15, 'create column', 'model : StructureModel<br />column_name : Detail<br />column_code : detail<br />column_main : false<br />column_field_type : richtext<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 7', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Detail'', ''detail'', ''false'', ''richtext'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''3'', ''7'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:32:59', '127.0.0.1'),
(108, 'mother_column', 16, 'create column', 'model : StructureModel<br />column_name : Date<br />column_code : date<br />column_main : false<br />column_field_type : date<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 4<br />table_id : 7', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Date'', ''date'', ''false'', ''date'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''4'', ''7'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:33:23', '127.0.0.1'),
(109, 'mother_column', 17, 'create column', 'model : StructureModel<br />column_name : Writer<br />column_code : writer<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 5<br />table_id : 7', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Writer'', ''writer'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''5'', ''7'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:33:46', '127.0.0.1'),
(110, 'mother_column', 18, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 6<br />table_id : 7', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''6'', ''7'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:34:01', '127.0.0.1'),
(111, 'mother_table', 7, 'update table', 'model : StructureModel<br /><b>Update</b><br />sort_priority : 1 => 4', 'UPDATE `mother_table` SET `sort_priority` = ''4'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''7''', 2, '2014-06-09 11:35:13', '127.0.0.1'),
(112, 'mother_table', 8, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : News<br />table_code : news<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 5', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''News'', ''news'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''5'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:35:37', '127.0.0.1'),
(113, 'mother_column', 19, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:35:55', '127.0.0.1'),
(114, 'mother_column', 20, 'create column', 'model : StructureModel<br />column_name : Title<br />column_code : title<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Title'', ''title'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:36:13', '127.0.0.1'),
(115, 'mother_column', 21, 'create column', 'model : StructureModel<br />column_name : Date<br />column_code : date<br />column_main : false<br />column_field_type : date<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Date'', ''date'', ''false'', ''date'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''3'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:36:50', '127.0.0.1'),
(116, 'mother_column', 22, 'create column', 'model : StructureModel<br />column_name : Time<br />column_code : time<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 4<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Time'', ''time'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''4'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:37:14', '127.0.0.1'),
(117, 'mother_column', 23, 'create column', 'model : StructureModel<br />column_name : Detail<br />column_code : detail<br />column_main : false<br />column_field_type : richtext<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 5<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Detail'', ''detail'', ''false'', ''richtext'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''5'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:37:33', '127.0.0.1'),
(118, 'mother_column', 24, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 6<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''6'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:37:47', '127.0.0.1'),
(119, 'mother_table', 9, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Contact us<br />table_code : contact<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 6', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Contact us'', ''contact'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''6'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 11:38:34', '127.0.0.1'),
(120, 'mother_column', 25, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 9', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''9'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 12:25:37', '127.0.0.1');
INSERT INTO `log_action` (`action_id`, `table_name`, `content_id`, `action_name`, `action_detail`, `action_query`, `create_by`, `create_date`, `create_ip`) VALUES
(121, 'mother_column', 26, 'create column', 'model : StructureModel<br />column_name : Lastname<br />column_code : lastname<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 9', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Lastname'', ''lastname'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''1'', ''9'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 12:25:52', '127.0.0.1'),
(122, 'mother_column', 26, 'delete column', 'model : StructureModel<br />column_id : 26<br />sort_priority : 1 => 2', 'UPDATE `mother_column` SET `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''2'' WHERE `column_id` =  ''26''', 2, '2014-06-09 12:25:55', '127.0.0.1'),
(123, 'mother_column', 27, 'create column', 'model : StructureModel<br />column_name : Tel<br />column_code : tel<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 9', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Tel'', ''tel'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''3'', ''9'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 12:26:07', '127.0.0.1'),
(124, 'mother_column', 28, 'create column', 'model : StructureModel<br />column_name : Email<br />column_code : email<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 4<br />table_id : 9', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Email'', ''email'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''4'', ''9'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 12:26:20', '127.0.0.1'),
(125, 'mother_column', 29, 'create column', 'model : StructureModel<br />column_name : Message<br />column_code : message<br />column_main : false<br />column_field_type : textarea<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 5<br />table_id : 9', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Message'', ''message'', ''false'', ''textarea'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''5'', ''9'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 12:26:46', '127.0.0.1'),
(126, 'mother_column', 29, 'delete column', 'model : StructureModel<br />column_id : 29', 'UPDATE `mother_column` SET `update_date` = NOW(), `update_by` = ''2'' WHERE `column_id` =  ''29''', 2, '2014-06-09 12:32:24', '127.0.0.1'),
(127, 'mother_table', 9, 'update table', 'model : StructureModel<br /><b>Update</b><br />table_newcontent : true => false', 'UPDATE `mother_table` SET `table_newcontent` = ''false'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-09 12:32:51', '127.0.0.1'),
(128, 'mother_column', 30, 'create column', 'model : StructureModel<br />column_name : Vdo<br />column_code : vdo<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 7<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Vdo'', ''vdo'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''7'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 12:58:09', '127.0.0.1'),
(129, 'tbl_home_slide', 1, 'insert Home Slide', 'model : BackendModel<br />name : image1<br />image : /userfiles/images/1.png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_home_slide` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''image1'', ''/userfiles/images/1.png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-09 13:09:11', '127.0.0.1'),
(130, 'tbl_home_slide_lang', 1, 'insert Home Slide_lang', 'model : BackendModel', 'INSERT INTO `tbl_home_slide_lang` (`home_slide_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-09 13:09:11', '127.0.0.1'),
(131, 'tbl_home_slide_lang', 1, 'insert Home Slide_lang', 'model : BackendModel', 'INSERT INTO `tbl_home_slide_lang` (`home_slide_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-09 13:09:11', '127.0.0.1'),
(132, 'tbl_home_slide', 2, 'insert Home Slide', 'model : BackendModel<br />name : image2<br />image : /userfiles/images/1.png<br />recursive_id : 0<br />sort_priority : 2<br />enable_status : show', 'INSERT INTO `tbl_home_slide` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''image2'', ''/userfiles/images/1.png'', 0, ''2'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-09 13:09:28', '127.0.0.1'),
(133, 'tbl_home_slide_lang', 2, 'insert Home Slide_lang', 'model : BackendModel', 'INSERT INTO `tbl_home_slide_lang` (`home_slide_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-09 13:09:28', '127.0.0.1'),
(134, 'tbl_home_slide_lang', 2, 'insert Home Slide_lang', 'model : BackendModel', 'INSERT INTO `tbl_home_slide_lang` (`home_slide_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-09 13:09:28', '127.0.0.1'),
(135, 'mother_column', 31, 'create column', 'model : StructureModel<br />column_name : Link<br />column_code : link<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 4', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Link'', ''link'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''4'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 13:59:33', '127.0.0.1'),
(136, 'tbl_home_slide', 1, 'update Home Slide', 'model : BackendModel<br /><b>Update</b><br />link :  => https://www.facebook.com/<br />sort_priority : 1 => 1', 'UPDATE `tbl_home_slide` SET `link` = ''https://www.facebook.com/'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `home_slide_id` =  ''1''', 2, '2014-06-09 13:59:43', '127.0.0.1'),
(137, 'tbl_home_slide_lang', 1, 'insert Home Slide_lang', 'model : BackendModel', 'UPDATE `tbl_home_slide_lang` SET `home_slide_id` = ''1'', `lang_id` = ''1'' WHERE `home_slide_lang_id` =  ''1''', 2, '2014-06-09 13:59:43', '127.0.0.1'),
(138, 'tbl_home_slide_lang', 2, 'insert Home Slide_lang', 'model : BackendModel', 'UPDATE `tbl_home_slide_lang` SET `home_slide_id` = ''1'', `lang_id` = ''2'' WHERE `home_slide_lang_id` =  ''2''', 2, '2014-06-09 13:59:43', '127.0.0.1'),
(139, 'tbl_news', 1, 'insert News', 'model : BackendModel<br />name : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์<br />title : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />date : 2014-06-09<br />time : 07:41<br />detail : <p>\r\n	<img alt="" src="/userfiles/images/img.png" style="width: 359px; height: 703px; margin-left: 15px; margin-right: 15px; float: left;" /></p>\r\n<p>\r\n	8 December 2012</p>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<br />image : /userfiles/images/thumb.png<br />vdo : <br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_news` (`name`, `title`, `date`, `time`, `detail`, `image`, `vdo`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์'', ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', ''2014-06-09'', ''07:41'', ''<p>\\r\\n	<img alt=\\"\\" src=\\"/userfiles/images/img.png\\" style=\\"width: 359px; height: 703px; margin-left: 15px; margin-right: 15px; float: left;\\" /></p>\\r\\n<p>\\r\\n	8 December 2012</p>\\r\\n<h4>\\r\\n	<span style=\\"color:#b22222;\\">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\\r\\n'', ''/userfiles/images/thumb.png'', '''', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-09 14:26:46', '127.0.0.1'),
(140, 'tbl_news_lang', 1, 'insert News_lang', 'model : BackendModel', 'INSERT INTO `tbl_news_lang` (`news_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-09 14:26:46', '127.0.0.1'),
(141, 'tbl_news_lang', 1, 'insert News_lang', 'model : BackendModel', 'INSERT INTO `tbl_news_lang` (`news_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-09 14:26:46', '127.0.0.1'),
(142, 'tbl_news', 1, 'update News', 'model : BackendModel<br /><b>Update</b><br />vdo :  => yZf2mlmsDvc<br />sort_priority : 1 => 1', 'UPDATE `tbl_news` SET `vdo` = ''yZf2mlmsDvc'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `news_id` =  ''1''', 2, '2014-06-09 14:26:58', '127.0.0.1'),
(143, 'tbl_news_lang', 1, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''1'', `lang_id` = ''1'' WHERE `news_lang_id` =  ''1''', 2, '2014-06-09 14:26:58', '127.0.0.1'),
(144, 'tbl_news_lang', 2, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''1'', `lang_id` = ''2'' WHERE `news_lang_id` =  ''2''', 2, '2014-06-09 14:26:58', '127.0.0.1'),
(145, 'tbl_news', 2, 'insert News', 'model : BackendModel<br />name : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์<br />title : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />date : 2014-06-13<br />time : 8.00<br />detail : <p>\r\n	<img alt="" src="/userfiles/images/img.png" style="width: 359px; height: 703px; margin-left: 15px; margin-right: 15px; float: left;" /></p>\r\n<p>\r\n	8 December 2012</p>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<br />image : /userfiles/images/thumb.png<br />vdo : <br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_news` (`name`, `title`, `date`, `time`, `detail`, `image`, `vdo`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์'', ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', ''2014-06-13'', ''8.00'', ''<p>\\r\\n	<img alt=\\"\\" src=\\"/userfiles/images/img.png\\" style=\\"width: 359px; height: 703px; margin-left: 15px; margin-right: 15px; float: left;\\" /></p>\\r\\n<p>\\r\\n	8 December 2012</p>\\r\\n<h4>\\r\\n	<span style=\\"color:#b22222;\\">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\\r\\n'', ''/userfiles/images/thumb.png'', '''', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-09 14:42:23', '127.0.0.1'),
(146, 'tbl_news_lang', 2, 'insert News_lang', 'model : BackendModel', 'INSERT INTO `tbl_news_lang` (`news_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-09 14:42:23', '127.0.0.1'),
(147, 'tbl_news_lang', 2, 'insert News_lang', 'model : BackendModel', 'INSERT INTO `tbl_news_lang` (`news_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-09 14:42:23', '127.0.0.1'),
(148, 'tbl_news', 2, 'update News', 'model : BackendModel<br /><b>Update</b><br />vdo :  => kqajGnxgNic<br />sort_priority : 1 => 1', 'UPDATE `tbl_news` SET `vdo` = ''kqajGnxgNic'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `news_id` =  ''2''', 2, '2014-06-09 14:42:43', '127.0.0.1'),
(149, 'tbl_news_lang', 3, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''2'', `lang_id` = ''1'' WHERE `news_lang_id` =  ''3''', 2, '2014-06-09 14:42:43', '127.0.0.1'),
(150, 'tbl_news_lang', 4, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''2'', `lang_id` = ''2'' WHERE `news_lang_id` =  ''4''', 2, '2014-06-09 14:42:43', '127.0.0.1'),
(151, 'mother_table', 10, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Calendar<br />table_code : calendar<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Calendar'', ''calendar'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''1'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 14:49:18', '127.0.0.1'),
(152, 'mother_column', 32, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 10', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''10'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 14:50:14', '127.0.0.1'),
(153, 'mother_column', 33, 'create column', 'model : StructureModel<br />column_name : Time<br />column_code : time<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 10', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Time'', ''time'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''10'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 14:50:36', '127.0.0.1'),
(154, 'mother_column', 34, 'create column', 'model : StructureModel<br />column_name : Date<br />column_code : date<br />column_main : false<br />column_field_type : date<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 10', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Date'', ''date'', ''false'', ''date'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''10'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 14:51:07', '127.0.0.1'),
(155, 'mother_column', 35, 'create column', 'model : StructureModel<br />column_name : Title<br />column_code : title<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 4<br />table_id : 10', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Title'', ''title'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''4'', ''10'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 14:51:26', '127.0.0.1');
INSERT INTO `log_action` (`action_id`, `table_name`, `content_id`, `action_name`, `action_detail`, `action_query`, `create_by`, `create_date`, `create_ip`) VALUES
(156, 'mother_column', 36, 'create column', 'model : StructureModel<br />column_name : Detail<br />column_code : detail<br />column_main : false<br />column_field_type : richtext<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 5<br />table_id : 10', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Detail'', ''detail'', ''false'', ''richtext'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''5'', ''10'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-09 14:51:59', '127.0.0.1'),
(157, 'mother_table', 10, 'update table', 'model : StructureModel<br /><b>Update</b><br />sort_priority : 1 => 7', 'UPDATE `mother_table` SET `sort_priority` = ''7'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''10''', 2, '2014-06-09 14:52:18', '127.0.0.1'),
(158, 'tbl_calendar', 1, 'insert Calendar', 'model : BackendModel<br />name : วันขอบคุณปวงเทพเทวะ<br />date : 2014-06-09<br />time : 10.30-12.00<br />title : <br />detail : <br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_calendar` (`name`, `date`, `time`, `title`, `detail`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วันขอบคุณปวงเทพเทวะ'', ''2014-06-09'', ''10.30-12.00'', '''', '''', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-09 15:05:41', '127.0.0.1'),
(159, 'tbl_calendar_lang', 1, 'insert Calendar_lang', 'model : BackendModel', 'INSERT INTO `tbl_calendar_lang` (`calendar_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-09 15:05:41', '127.0.0.1'),
(160, 'tbl_calendar_lang', 1, 'insert Calendar_lang', 'model : BackendModel', 'INSERT INTO `tbl_calendar_lang` (`calendar_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-09 15:05:41', '127.0.0.1'),
(161, 'tbl_calendar', 1, 'update Calendar', 'model : BackendModel<br /><b>Update</b><br />title :  => ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />detail :  => <p>\r\n	ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ</p>\r\n<br />sort_priority : 1 => 1', 'UPDATE `tbl_calendar` SET `title` = ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', `detail` = ''<p>\\r\\n	ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ</p>\\r\\n'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `calendar_id` =  ''1''', 2, '2014-06-09 15:06:06', '127.0.0.1'),
(162, 'tbl_calendar_lang', 1, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''1'', `lang_id` = ''1'' WHERE `calendar_lang_id` =  ''1''', 2, '2014-06-09 15:06:06', '127.0.0.1'),
(163, 'tbl_calendar_lang', 2, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''1'', `lang_id` = ''2'' WHERE `calendar_lang_id` =  ''2''', 2, '2014-06-09 15:06:06', '127.0.0.1'),
(164, 'tbl_calendar', 2, 'insert Calendar', 'model : BackendModel<br />name : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์<br />date : 2014-06-02<br />time : 10.30-19.00<br />title : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />detail : <p>\r\n	ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ</p>\r\n<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_calendar` (`name`, `date`, `time`, `title`, `detail`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์'', ''2014-06-02'', ''10.30-19.00'', ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', ''<p>\\r\\n	ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ</p>\\r\\n'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-09 15:06:36', '127.0.0.1'),
(165, 'tbl_calendar_lang', 2, 'insert Calendar_lang', 'model : BackendModel', 'INSERT INTO `tbl_calendar_lang` (`calendar_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-09 15:06:36', '127.0.0.1'),
(166, 'tbl_calendar_lang', 2, 'insert Calendar_lang', 'model : BackendModel', 'INSERT INTO `tbl_calendar_lang` (`calendar_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-09 15:06:36', '127.0.0.1'),
(167, 'tbl_calendar', 1, 'update Calendar', 'model : BackendModel<br /><b>Update</b><br />date : 2014-06-09 => 2014-05-06<br />sort_priority : 2 => 2', 'UPDATE `tbl_calendar` SET `date` = ''2014-05-06'', `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''2'' WHERE `calendar_id` =  ''1''', 2, '2014-06-09 16:07:09', '127.0.0.1'),
(168, 'tbl_calendar_lang', 1, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''1'', `lang_id` = ''1'' WHERE `calendar_lang_id` =  ''1''', 2, '2014-06-09 16:07:09', '127.0.0.1'),
(169, 'tbl_calendar_lang', 2, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''1'', `lang_id` = ''2'' WHERE `calendar_lang_id` =  ''2''', 2, '2014-06-09 16:07:09', '127.0.0.1'),
(170, 'tbl_calendar', 2, 'update Calendar', 'model : BackendModel<br /><b>Update</b><br />date : 2014-06-02 => 2014-05-07<br />sort_priority : 1 => 1', 'UPDATE `tbl_calendar` SET `date` = ''2014-05-07'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `calendar_id` =  ''2''', 2, '2014-06-09 16:10:06', '127.0.0.1'),
(171, 'tbl_calendar_lang', 3, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''2'', `lang_id` = ''1'' WHERE `calendar_lang_id` =  ''3''', 2, '2014-06-09 16:10:06', '127.0.0.1'),
(172, 'tbl_calendar_lang', 4, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''2'', `lang_id` = ''2'' WHERE `calendar_lang_id` =  ''4''', 2, '2014-06-09 16:10:06', '127.0.0.1'),
(173, 'tbl_calendar', 1, 'update Calendar', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 2 => 2', 'UPDATE `tbl_calendar` SET `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''2'' WHERE `calendar_id` =  ''1''', 2, '2014-06-09 16:37:23', '127.0.0.1'),
(174, 'tbl_calendar', 1, 'update Calendar', 'model : BackendModel<br /><b>Update</b><br />date : 2014-06-12 => 2014-08-06<br />sort_priority : 2 => 2', 'UPDATE `tbl_calendar` SET `date` = ''2014-08-06'', `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''2'' WHERE `calendar_id` =  ''1''', 2, '2014-06-09 16:37:45', '127.0.0.1'),
(175, 'tbl_calendar_lang', 1, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''1'', `lang_id` = ''1'' WHERE `calendar_lang_id` =  ''1''', 2, '2014-06-09 16:37:45', '127.0.0.1'),
(176, 'tbl_calendar_lang', 2, 'insert Calendar_lang', 'model : BackendModel', 'UPDATE `tbl_calendar_lang` SET `calendar_id` = ''1'', `lang_id` = ''2'' WHERE `calendar_lang_id` =  ''2''', 2, '2014-06-09 16:37:45', '127.0.0.1'),
(177, 'tbl_news', 3, 'insert News', 'model : BackendModel<br />name : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์<br />title : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />date : 2014-06-10<br />time : 10.30-19.00<br />detail : <p>\r\n	d</p>\r\n<br />image : /userfiles/images/thumb.png<br />vdo : kqajGnxgNic<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_news` (`name`, `title`, `date`, `time`, `detail`, `image`, `vdo`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์'', ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', ''2014-06-10'', ''10.30-19.00'', ''<p>\\r\\n	d</p>\\r\\n'', ''/userfiles/images/thumb.png'', ''kqajGnxgNic'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-10 09:53:55', '127.0.0.1'),
(178, 'tbl_news_lang', 3, 'insert News_lang', 'model : BackendModel', 'INSERT INTO `tbl_news_lang` (`news_id`, `lang_id`) VALUES (''3'', ''1'')', 2, '2014-06-10 09:53:55', '127.0.0.1'),
(179, 'tbl_news_lang', 3, 'insert News_lang', 'model : BackendModel', 'INSERT INTO `tbl_news_lang` (`news_id`, `lang_id`) VALUES (''3'', ''2'')', 2, '2014-06-10 09:53:55', '127.0.0.1'),
(180, 'tbl_news', 3, 'update News', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/thumb.png => /userfiles/images/1(1).png<br />sort_priority : 1 => 1', 'UPDATE `tbl_news` SET `image` = ''/userfiles/images/1(1).png'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `news_id` =  ''3''', 2, '2014-06-10 13:22:23', '127.0.0.1'),
(181, 'tbl_news_lang', 5, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''3'', `lang_id` = ''1'' WHERE `news_lang_id` =  ''5''', 2, '2014-06-10 13:22:23', '127.0.0.1'),
(182, 'tbl_news_lang', 6, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''3'', `lang_id` = ''2'' WHERE `news_lang_id` =  ''6''', 2, '2014-06-10 13:22:23', '127.0.0.1'),
(183, 'mother_column', 24, 'delete column', 'model : StructureModel<br />column_id : 24', 'DELETE FROM `mother_column`\nWHERE `column_id` =  ''24''', 2, '2014-06-10 13:53:22', '127.0.0.1'),
(184, 'mother_table', 11, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Vdo<br />table_code : vdo<br />table_type : static<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Vdo'', ''vdo'', ''static'', ''desc'', ''true'', ''false'', '''', ''1'', ''1'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 13:54:29', '127.0.0.1'),
(185, 'mother_table', 11, 'update table', 'model : StructureModel<br /><b>Update</b><br />sort_priority : 1 => 7', 'UPDATE `mother_table` SET `sort_priority` = ''7'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''11''', 2, '2014-06-10 13:54:38', '127.0.0.1'),
(186, 'mother_column', 37, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 11', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''11'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 13:55:55', '127.0.0.1'),
(187, 'mother_column', 38, 'create column', 'model : StructureModel<br />column_name : Url<br />column_code : url<br />column_main : false<br />column_field_type : url<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 11', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Url'', ''url'', ''false'', ''url'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''11'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 13:56:13', '127.0.0.1'),
(188, 'mother_column', 39, 'create column', 'model : StructureModel<br />column_name : Date<br />column_code : date<br />column_main : false<br />column_field_type : date<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 11', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Date'', ''date'', ''false'', ''date'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''3'', ''11'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 13:57:20', '127.0.0.1'),
(189, 'mother_column', 30, 'delete column', 'model : StructureModel<br />column_id : 30', 'DELETE FROM `mother_column`\nWHERE `column_id` =  ''30''', 2, '2014-06-10 13:57:47', '127.0.0.1'),
(190, 'mother_column', 40, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 6<br />table_id : 8', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''6'', ''8'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 13:58:09', '127.0.0.1'),
(191, 'tbl_news', 3, 'update News', 'model : BackendModel<br /><b>Update</b><br />image :  => /userfiles/images/thumb.png<br />sort_priority : 1 => 1', 'UPDATE `tbl_news` SET `image` = ''/userfiles/images/thumb.png'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `news_id` =  ''3''', 2, '2014-06-10 13:59:01', '127.0.0.1'),
(192, 'tbl_news_lang', 5, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''3'', `lang_id` = ''1'' WHERE `news_lang_id` =  ''5''', 2, '2014-06-10 13:59:01', '127.0.0.1'),
(193, 'tbl_news_lang', 6, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''3'', `lang_id` = ''2'' WHERE `news_lang_id` =  ''6''', 2, '2014-06-10 13:59:01', '127.0.0.1'),
(194, 'tbl_news', 2, 'update News', 'model : BackendModel<br /><b>Update</b><br />image :  => /userfiles/images/1(1).png<br />sort_priority : 2 => 2', 'UPDATE `tbl_news` SET `image` = ''/userfiles/images/1(1).png'', `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''2'' WHERE `news_id` =  ''2''', 2, '2014-06-10 13:59:12', '127.0.0.1'),
(195, 'tbl_news_lang', 3, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''2'', `lang_id` = ''1'' WHERE `news_lang_id` =  ''3''', 2, '2014-06-10 13:59:12', '127.0.0.1'),
(196, 'tbl_news_lang', 4, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''2'', `lang_id` = ''2'' WHERE `news_lang_id` =  ''4''', 2, '2014-06-10 13:59:12', '127.0.0.1'),
(197, 'tbl_news', 1, 'update News', 'model : BackendModel<br /><b>Update</b><br />image :  => /userfiles/images/1(1).png<br />sort_priority : 3 => 3', 'UPDATE `tbl_news` SET `image` = ''/userfiles/images/1(1).png'', `sort_priority` = ''3'', `update_date` = NOW(), `update_by` = ''2'' WHERE `news_id` =  ''1''', 2, '2014-06-10 13:59:22', '127.0.0.1'),
(198, 'tbl_news_lang', 1, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''1'', `lang_id` = ''1'' WHERE `news_lang_id` =  ''1''', 2, '2014-06-10 13:59:22', '127.0.0.1'),
(199, 'tbl_news_lang', 2, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''1'', `lang_id` = ''2'' WHERE `news_lang_id` =  ''2''', 2, '2014-06-10 13:59:22', '127.0.0.1'),
(200, 'tbl_news', 1, 'update News', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 3 => 3', 'UPDATE `tbl_news` SET `sort_priority` = ''3'', `update_date` = NOW(), `update_by` = ''2'' WHERE `news_id` =  ''1''', 2, '2014-06-10 13:59:27', '127.0.0.1'),
(201, 'tbl_news_lang', 1, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''1'', `lang_id` = ''1'' WHERE `news_lang_id` =  ''1''', 2, '2014-06-10 13:59:27', '127.0.0.1'),
(202, 'tbl_news_lang', 2, 'insert News_lang', 'model : BackendModel', 'UPDATE `tbl_news_lang` SET `news_id` = ''1'', `lang_id` = ''2'' WHERE `news_lang_id` =  ''2''', 2, '2014-06-10 13:59:27', '127.0.0.1'),
(203, 'tbl_vdo', 1, 'insert Vdo', 'model : BackendModel<br />name : วันขอบคุณปวงเทพเทวะ<br />url : http://youtu.be/kD4htT_qDmE<br />date : 2014-06-10<br />recursive_id : 0', 'INSERT INTO `tbl_vdo` (`name`, `url`, `date`, `recursive_id`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วันขอบคุณปวงเทพเทวะ'', ''http://youtu.be/kD4htT_qDmE'', ''2014-06-10'', 0, NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-10 14:04:30', '127.0.0.1'),
(204, 'tbl_vdo_lang', 1, 'insert Vdo_lang', 'model : BackendModel', 'INSERT INTO `tbl_vdo_lang` (`vdo_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-10 14:04:30', '127.0.0.1'),
(205, 'tbl_vdo_lang', 1, 'insert Vdo_lang', 'model : BackendModel', 'INSERT INTO `tbl_vdo_lang` (`vdo_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-10 14:04:30', '127.0.0.1'),
(206, 'mother_table', 11, 'update table', 'model : StructureModel<br /><b>Update</b><br />table_type : static => dynamic', 'UPDATE `mother_table` SET `table_type` = ''dynamic'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''11''', 2, '2014-06-10 14:04:46', '127.0.0.1'),
(207, 'tbl_vdo', 2, 'insert Vdo', 'model : BackendModel<br />name : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์<br />url : http://youtu.be/kD4htT_qDmE<br />date : 2014-06-23<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_vdo` (`name`, `url`, `date`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์'', ''http://youtu.be/kD4htT_qDmE'', ''2014-06-23'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-10 14:05:28', '127.0.0.1'),
(208, 'tbl_vdo_lang', 2, 'insert Vdo_lang', 'model : BackendModel', 'INSERT INTO `tbl_vdo_lang` (`vdo_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-10 14:05:28', '127.0.0.1'),
(209, 'tbl_vdo_lang', 2, 'insert Vdo_lang', 'model : BackendModel', 'INSERT INTO `tbl_vdo_lang` (`vdo_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-10 14:05:28', '127.0.0.1'),
(210, 'mother_column', 38, 'delete column', 'model : StructureModel<br />column_id : 38<br />column_name : Url => Embed<br />column_code : url => embed', 'UPDATE `mother_column` SET `column_name` = ''Embed'', `column_code` = ''embed'', `update_date` = NOW(), `update_by` = ''2'' WHERE `column_id` =  ''38''', 2, '2014-06-10 14:27:44', '127.0.0.1'),
(211, 'tbl_vdo', 1, 'update Vdo', 'model : BackendModel<br /><b>Update</b><br />embed : http://youtu.be/kD4htT_qDmE => kD4htT_qDmE<br />sort_priority : 0 => 1', 'UPDATE `tbl_vdo` SET `embed` = ''kD4htT_qDmE'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `vdo_id` =  ''1''', 2, '2014-06-10 14:28:09', '127.0.0.1'),
(212, 'tbl_vdo_lang', 1, 'insert Vdo_lang', 'model : BackendModel', 'UPDATE `tbl_vdo_lang` SET `vdo_id` = ''1'', `lang_id` = ''1'' WHERE `vdo_lang_id` =  ''1''', 2, '2014-06-10 14:28:09', '127.0.0.1'),
(213, 'tbl_vdo_lang', 2, 'insert Vdo_lang', 'model : BackendModel', 'UPDATE `tbl_vdo_lang` SET `vdo_id` = ''1'', `lang_id` = ''2'' WHERE `vdo_lang_id` =  ''2''', 2, '2014-06-10 14:28:09', '127.0.0.1'),
(214, 'tbl_vdo', 2, 'update Vdo', 'model : BackendModel<br /><b>Update</b><br />embed : http://youtu.be/kD4htT_qDmE => kD4htT_qDmE<br />sort_priority : 0 => 1', 'UPDATE `tbl_vdo` SET `embed` = ''kD4htT_qDmE'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `vdo_id` =  ''2''', 2, '2014-06-10 14:28:21', '127.0.0.1'),
(215, 'tbl_vdo_lang', 3, 'insert Vdo_lang', 'model : BackendModel', 'UPDATE `tbl_vdo_lang` SET `vdo_id` = ''2'', `lang_id` = ''1'' WHERE `vdo_lang_id` =  ''3''', 2, '2014-06-10 14:28:21', '127.0.0.1'),
(216, 'tbl_vdo_lang', 4, 'insert Vdo_lang', 'model : BackendModel', 'UPDATE `tbl_vdo_lang` SET `vdo_id` = ''2'', `lang_id` = ''2'' WHERE `vdo_lang_id` =  ''4''', 2, '2014-06-10 14:28:21', '127.0.0.1'),
(217, 'mother_table', 12, 'create table', 'model : StructureModel<br />parent_table_id : 8<br />table_name : Gallery_news<br />table_code : gallery_news<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''8'', ''Gallery_news'', ''gallery_news'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''1'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 14:46:29', '127.0.0.1'),
(218, 'mother_column', 41, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 12', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''12'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 14:48:24', '127.0.0.1'),
(219, 'mother_column', 42, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 12', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''12'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-10 14:48:43', '127.0.0.1'),
(220, 'tbl_gallery_news', 1, 'insert Gallery_news', 'model : BackendModel<br />parent_id : 3<br />name : image1<br />image : /userfiles/images/1(1).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_gallery_news` (`parent_id`, `name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''3'', ''image1'', ''/userfiles/images/1(1).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-10 14:49:34', '127.0.0.1'),
(221, 'tbl_gallery_news_lang', 1, 'insert Gallery_news_lang', 'model : BackendModel', 'INSERT INTO `tbl_gallery_news_lang` (`gallery_news_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-10 14:49:34', '127.0.0.1'),
(222, 'tbl_gallery_news_lang', 1, 'insert Gallery_news_lang', 'model : BackendModel', 'INSERT INTO `tbl_gallery_news_lang` (`gallery_news_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-10 14:49:34', '127.0.0.1'),
(223, 'tbl_gallery_news', 2, 'insert Gallery_news', 'model : BackendModel<br />parent_id : 3<br />name : image2<br />image : /userfiles/images/thumb.png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_gallery_news` (`parent_id`, `name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''3'', ''image2'', ''/userfiles/images/thumb.png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-10 14:49:47', '127.0.0.1'),
(224, 'tbl_gallery_news_lang', 2, 'insert Gallery_news_lang', 'model : BackendModel', 'INSERT INTO `tbl_gallery_news_lang` (`gallery_news_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-10 14:49:47', '127.0.0.1'),
(225, 'tbl_gallery_news_lang', 2, 'insert Gallery_news_lang', 'model : BackendModel', 'INSERT INTO `tbl_gallery_news_lang` (`gallery_news_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-10 14:49:47', '127.0.0.1'),
(226, 'tbl_gallery_news', 3, 'insert Gallery_news', 'model : BackendModel<br />parent_id : 3<br />name : image3<br />image : /userfiles/images/1(1).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_gallery_news` (`parent_id`, `name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''3'', ''image3'', ''/userfiles/images/1(1).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-10 14:50:17', '127.0.0.1'),
(227, 'tbl_gallery_news_lang', 3, 'insert Gallery_news_lang', 'model : BackendModel', 'INSERT INTO `tbl_gallery_news_lang` (`gallery_news_id`, `lang_id`) VALUES (''3'', ''1'')', 2, '2014-06-10 14:50:17', '127.0.0.1'),
(228, 'tbl_gallery_news_lang', 3, 'insert Gallery_news_lang', 'model : BackendModel', 'INSERT INTO `tbl_gallery_news_lang` (`gallery_news_id`, `lang_id`) VALUES (''3'', ''2'')', 2, '2014-06-10 14:50:17', '127.0.0.1'),
(229, 'mother_table', 5, 'update table', 'model : StructureModel<br /><b>Update</b><br />table_type : dynamic => static', 'UPDATE `mother_table` SET `table_type` = ''static'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''5''', 2, '2014-06-11 15:32:31', '127.0.0.1'),
(230, 'mother_column', 7, 'delete column', 'model : StructureModel<br />column_id : 7', 'DELETE FROM `mother_column`\nWHERE `column_id` =  ''7''', 2, '2014-06-11 15:33:45', '127.0.0.1'),
(231, 'mother_column', 8, 'delete column', 'model : StructureModel<br />column_id : 8', 'DELETE FROM `mother_column`\nWHERE `column_id` =  ''8''', 2, '2014-06-11 15:33:50', '127.0.0.1'),
(232, 'mother_column', 10, 'delete column', 'model : StructureModel<br />column_id : 10', 'DELETE FROM `mother_column`\nWHERE `column_id` =  ''10''', 2, '2014-06-11 15:33:55', '127.0.0.1'),
(233, 'tbl_about', 1, 'insert About us', 'model : BackendModel<br />detail : <p style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/img(1).png" style="width: 813px; height: 453px;" /></p>\r\n<p>\r\n	พุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ | 紫峰閣) ก่อตั้งเมื่อปี พ.ศ. 2499 CHEE HONG BUDDHIST ASSOCIATION OF THAILAND Founded in B.E.2499 ตั้งอยู่ที่ หมู่ 9 ถ.สุขประยูร ต.บ้านสวน อ.เมืองชลบุรี ในราวๆ ปี พ.ศ.2492 ได้มี เศรษฐีชาวจีนผู้หนึ่งซึ่งมีภูมิลำเนาอยู่ในจังหวัดพระนครได้เที่ยวเสาะแสวงหาที่ดินที่มีทำเลเหมาะเพื่อจัดสร้าง</p>\r\n<br />recursive_id : 0', 'INSERT INTO `tbl_about` (`detail`, `recursive_id`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''<p style=\\"text-align: center;\\">\\r\\n	<img alt=\\"\\" src=\\"/userfiles/images/img(1).png\\" style=\\"width: 813px; height: 453px;\\" /></p>\\r\\n<p>\\r\\n	พุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ | 紫峰閣) ก่อตั้งเมื่อปี พ.ศ. 2499 CHEE HONG BUDDHIST ASSOCIATION OF THAILAND Founded in B.E.2499 ตั้งอยู่ที่ หมู่ 9 ถ.สุขประยูร ต.บ้านสวน อ.เมืองชลบุรี ในราวๆ ปี พ.ศ.2492 ได้มี เศรษฐีชาวจีนผู้หนึ่งซึ่งมีภูมิลำเนาอยู่ในจังหวัดพระนครได้เที่ยวเสาะแสวงหาที่ดินที่มีทำเลเหมาะเพื่อจัดสร้าง</p>\\r\\n'', 0, NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 15:34:57', '127.0.0.1'),
(234, 'tbl_about_lang', 1, 'insert About us_lang', 'model : BackendModel', 'INSERT INTO `tbl_about_lang` (`about_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-11 15:34:57', '127.0.0.1'),
(235, 'tbl_about_lang', 1, 'insert About us_lang', 'model : BackendModel', 'INSERT INTO `tbl_about_lang` (`about_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-11 15:34:57', '127.0.0.1'),
(236, 'mother_table', 13, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : About_list<br />table_code : about_list<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''About_list'', ''about_list'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''1'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 15:40:59', '127.0.0.1'),
(237, 'mother_column', 43, 'create column', 'model : StructureModel<br />column_name : Date<br />column_code : date<br />column_main : true<br />column_field_type : date<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 13', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Date'', ''date'', ''true'', ''date'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''13'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 15:41:31', '127.0.0.1'),
(238, 'mother_column', 44, 'create column', 'model : StructureModel<br />column_name : Title<br />column_code : title<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 13', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Title'', ''title'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''13'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 15:41:50', '127.0.0.1'),
(239, 'mother_column', 45, 'create column', 'model : StructureModel<br />column_name : Image<br />column_code : image<br />column_main : false<br />column_field_type : image<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 13', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Image'', ''image'', ''false'', ''image'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''3'', ''13'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 15:42:06', '127.0.0.1'),
(240, 'mother_table', 13, 'update table', 'model : StructureModel<br /><b>Update</b><br />parent_table_id : 0 => 5', 'UPDATE `mother_table` SET `parent_table_id` = ''5'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''13''', 2, '2014-06-11 15:42:42', '127.0.0.1'),
(241, 'tbl_about_list', 1, 'insert About_list', 'model : BackendModel<br />parent_id : 1<br />date : 2014-06-01<br />title : สมเด็จพระอริยวงศาคตญาณ สมเด็จพระสังฆราชสกลมหาสังฆปริณายก (อยู่ญาโณทยมหาเถร) วัดสระเกศราชวรมหาวิหาร สมเด็จพระสังฆราชพระองค์ที่ 15 แห่งกรุงรัตนโกสินทร์เสด็จเป็นองค์ ประธานในงานเปิด พุทธวิหารพุทธสมาคมจี่ฮง แห่งประเทศไทย พร้อมทั้งทรงประทานพระฉายาลักษณ์ แด่พุทธสมาคมฯ<br />image : /userfiles/images/1(2).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_about_list` (`parent_id`, `date`, `title`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''1'', ''2014-06-01'', ''สมเด็จพระอริยวงศาคตญาณ สมเด็จพระสังฆราชสกลมหาสังฆปริณายก (อยู่ญาโณทยมหาเถร) วัดสระเกศราชวรมหาวิหาร สมเด็จพระสังฆราชพระองค์ที่ 15 แห่งกรุงรัตนโกสินทร์เสด็จเป็นองค์ ประธานในงานเปิด พุทธวิหารพุทธสมาคมจี่ฮง แห่งประเทศไทย พร้อมทั้งทรงประทานพระฉายาลักษณ์ แด่พุทธสมาคมฯ'', ''/userfiles/images/1(2).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 15:43:11', '127.0.0.1'),
(242, 'tbl_about_list_lang', 1, 'insert About_list_lang', 'model : BackendModel', 'INSERT INTO `tbl_about_list_lang` (`about_list_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-11 15:43:11', '127.0.0.1'),
(243, 'tbl_about_list_lang', 1, 'insert About_list_lang', 'model : BackendModel', 'INSERT INTO `tbl_about_list_lang` (`about_list_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-11 15:43:11', '127.0.0.1'),
(244, 'tbl_about_list', 2, 'insert About_list', 'model : BackendModel<br />parent_id : 1<br />date : 2014-06-11<br />title : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />image : /userfiles/images/2.png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_about_list` (`parent_id`, `date`, `title`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''1'', ''2014-06-11'', ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', ''/userfiles/images/2.png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 15:43:37', '127.0.0.1'),
(245, 'tbl_about_list_lang', 2, 'insert About_list_lang', 'model : BackendModel', 'INSERT INTO `tbl_about_list_lang` (`about_list_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-11 15:43:37', '127.0.0.1'),
(246, 'tbl_about_list_lang', 2, 'insert About_list_lang', 'model : BackendModel', 'INSERT INTO `tbl_about_list_lang` (`about_list_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-11 15:43:37', '127.0.0.1'),
(247, 'tbl_inside', 1, 'insert Inside', 'model : BackendModel<br />name : วิหารเต๋าโจ้ว<br />image : /userfiles/images/1(3).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารเต๋าโจ้ว'', ''/userfiles/images/1(3).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:02:00', '127.0.0.1'),
(248, 'tbl_inside_lang', 1, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-11 17:02:00', '127.0.0.1'),
(249, 'tbl_inside_lang', 1, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-11 17:02:00', '127.0.0.1'),
(250, 'tbl_inside', 2, 'insert Inside', 'model : BackendModel<br />name : วิหารเต๋าโจ้ว<br />image : /userfiles/images/1(3).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารเต๋าโจ้ว'', ''/userfiles/images/1(3).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:02:15', '127.0.0.1'),
(251, 'tbl_inside_lang', 2, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-11 17:02:15', '127.0.0.1'),
(252, 'tbl_inside_lang', 2, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-11 17:02:15', '127.0.0.1'),
(253, 'tbl_inside', 3, 'insert Inside', 'model : BackendModel<br />name : วิหารเต๋าโจ้ว<br />image : /userfiles/images/1(3).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารเต๋าโจ้ว'', ''/userfiles/images/1(3).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:02:26', '127.0.0.1'),
(254, 'tbl_inside_lang', 3, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''3'', ''1'')', 2, '2014-06-11 17:02:26', '127.0.0.1'),
(255, 'tbl_inside_lang', 3, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''3'', ''2'')', 2, '2014-06-11 17:02:26', '127.0.0.1'),
(256, 'tbl_inside', 4, 'insert Inside', 'model : BackendModel<br />name : วิหารเต๋าโจ้ว<br />image : /userfiles/images/1(3).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารเต๋าโจ้ว'', ''/userfiles/images/1(3).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:02:38', '127.0.0.1'),
(257, 'tbl_inside_lang', 4, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''4'', ''1'')', 2, '2014-06-11 17:02:38', '127.0.0.1'),
(258, 'tbl_inside_lang', 4, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''4'', ''2'')', 2, '2014-06-11 17:02:38', '127.0.0.1'),
(259, 'tbl_inside', 5, 'insert Inside', 'model : BackendModel<br />name : วิหารเต๋าโจ้ว<br />image : /userfiles/images/1(3).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารเต๋าโจ้ว'', ''/userfiles/images/1(3).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:02:47', '127.0.0.1'),
(260, 'tbl_inside_lang', 5, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''5'', ''1'')', 2, '2014-06-11 17:02:47', '127.0.0.1'),
(261, 'tbl_inside_lang', 5, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''5'', ''2'')', 2, '2014-06-11 17:02:47', '127.0.0.1'),
(262, 'tbl_inside', 6, 'insert Inside', 'model : BackendModel<br />name : วิหารเต๋าโจ้ว<br />image : /userfiles/images/1(3).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารเต๋าโจ้ว'', ''/userfiles/images/1(3).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:06:26', '127.0.0.1'),
(263, 'tbl_inside_lang', 6, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''6'', ''1'')', 2, '2014-06-11 17:06:26', '127.0.0.1'),
(264, 'tbl_inside_lang', 6, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''6'', ''2'')', 2, '2014-06-11 17:06:26', '127.0.0.1');
INSERT INTO `log_action` (`action_id`, `table_name`, `content_id`, `action_name`, `action_detail`, `action_query`, `create_by`, `create_date`, `create_ip`) VALUES
(265, 'tbl_article', 1, 'insert Article', 'model : BackendModel<br />name : พิธีวางศิลาฤกษ์มหาเจดีย์"พุทธรังษีรัตนเจดีย์"<br />title : เมื่อวันเสาร์ที่ 8 ธันวาคม นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี  โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ)<br />detail : <br />date : 2014-06-11<br />writer : Administrator <br />image : /userfiles/images/2(1).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_article` (`name`, `title`, `detail`, `date`, `writer`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''พิธีวางศิลาฤกษ์มหาเจดีย์\\"พุทธรังษีรัตนเจดีย์\\"'', ''เมื่อวันเสาร์ที่ 8 ธันวาคม นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี  โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ)'', '''', ''2014-06-11'', ''Administrator '', ''/userfiles/images/2(1).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:30:03', '127.0.0.1'),
(266, 'tbl_article_lang', 1, 'insert Article_lang', 'model : BackendModel', 'INSERT INTO `tbl_article_lang` (`article_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-11 17:30:03', '127.0.0.1'),
(267, 'tbl_article_lang', 1, 'insert Article_lang', 'model : BackendModel', 'INSERT INTO `tbl_article_lang` (`article_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-11 17:30:03', '127.0.0.1'),
(268, 'tbl_article', 1, 'update Article', 'model : BackendModel<br /><b>Update</b><br />name : พิธีวางศิลาฤกษ์มหาเจดีย์"พุทธรังษีรัตนเจดีย์" => พิธีวางศิลาฤกษ์มหาเจดีย์<br />detail :  => <h4 style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/1(4).png" style="width: 891px; height: 346px;" /></h4>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\r\n<br />\r\n<br />\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\r\n<br />sort_priority : 1 => 1', 'UPDATE `tbl_article` SET `name` = ''พิธีวางศิลาฤกษ์มหาเจดีย์'', `detail` = ''<h4 style=\\"text-align: center;\\">\\r\\n	<img alt=\\"\\" src=\\"/userfiles/images/1(4).png\\" style=\\"width: 891px; height: 346px;\\" /></h4>\\r\\n<h4>\\r\\n	<span style=\\"color:#b22222;\\">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\\r\\n<p>\\r\\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\\r\\n<br />\\r\\n<br />\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\\r\\n	<br />\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\\r\\n	<br />\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\\r\\n'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `article_id` =  ''1''', 2, '2014-06-11 17:31:31', '127.0.0.1'),
(269, 'tbl_article_lang', 1, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''1'', `lang_id` = ''1'' WHERE `article_lang_id` =  ''1''', 2, '2014-06-11 17:31:31', '127.0.0.1'),
(270, 'tbl_article_lang', 2, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''1'', `lang_id` = ''2'' WHERE `article_lang_id` =  ''2''', 2, '2014-06-11 17:31:31', '127.0.0.1'),
(271, 'tbl_article', 2, 'insert Article', 'model : BackendModel<br />name : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์<br />title : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />detail : <h4 style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/1(4).png" style="width: 891px; height: 346px;" /></h4>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\r\n<br />\r\n<br />\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\r\n<br />date : 2014-06-11<br />writer : Administrator<br />image : /userfiles/images/2(1).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_article` (`name`, `title`, `detail`, `date`, `writer`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์'', ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', ''<h4 style=\\"text-align: center;\\">\\r\\n	<img alt=\\"\\" src=\\"/userfiles/images/1(4).png\\" style=\\"width: 891px; height: 346px;\\" /></h4>\\r\\n<h4>\\r\\n	<span style=\\"color:#b22222;\\">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\\r\\n<p>\\r\\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\\r\\n<br />\\r\\n<br />\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\\r\\n	<br />\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\\r\\n	<br />\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\\r\\n'', ''2014-06-11'', ''Administrator'', ''/userfiles/images/2(1).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:32:10', '127.0.0.1'),
(272, 'tbl_article_lang', 2, 'insert Article_lang', 'model : BackendModel', 'INSERT INTO `tbl_article_lang` (`article_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-11 17:32:10', '127.0.0.1'),
(273, 'tbl_article_lang', 2, 'insert Article_lang', 'model : BackendModel', 'INSERT INTO `tbl_article_lang` (`article_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-11 17:32:10', '127.0.0.1'),
(274, 'tbl_article', 2, 'update Article', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 1 => 1', 'UPDATE `tbl_article` SET `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `article_id` =  ''2''', 2, '2014-06-11 17:32:13', '127.0.0.1'),
(275, 'tbl_article_lang', 3, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''2'', `lang_id` = ''1'' WHERE `article_lang_id` =  ''3''', 2, '2014-06-11 17:32:13', '127.0.0.1'),
(276, 'tbl_article_lang', 4, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''2'', `lang_id` = ''2'' WHERE `article_lang_id` =  ''4''', 2, '2014-06-11 17:32:13', '127.0.0.1');
INSERT INTO `log_action` (`action_id`, `table_name`, `content_id`, `action_name`, `action_detail`, `action_query`, `create_by`, `create_date`, `create_ip`) VALUES
(277, 'tbl_article', 3, 'insert Article', 'model : BackendModel<br />name : วิหารเต๋าโจ้ว<br />title : ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ<br />detail : <h4 style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/1(4).png" style="width: 891px; height: 346px;" /></h4>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\r\n<br />\r\n<br />\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\r\n<br />date : 2014-06-11<br />writer : Administrator<br />image : /userfiles/images/2(1).png<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_article` (`name`, `title`, `detail`, `date`, `writer`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารเต๋าโจ้ว'', ''ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ'', ''<h4 style=\\"text-align: center;\\">\\r\\n	<img alt=\\"\\" src=\\"/userfiles/images/1(4).png\\" style=\\"width: 891px; height: 346px;\\" /></h4>\\r\\n<h4>\\r\\n	<span style=\\"color:#b22222;\\">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\\r\\n<p>\\r\\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\\r\\n<br />\\r\\n<br />\\r\\n<p>\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\\r\\n	<br />\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\\r\\n	<br />\\r\\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\\r\\n'', ''2014-06-11'', ''Administrator'', ''/userfiles/images/2(1).png'', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:32:50', '127.0.0.1'),
(278, 'tbl_article_lang', 3, 'insert Article_lang', 'model : BackendModel', 'INSERT INTO `tbl_article_lang` (`article_id`, `lang_id`) VALUES (''3'', ''1'')', 2, '2014-06-11 17:32:50', '127.0.0.1'),
(279, 'tbl_article_lang', 3, 'insert Article_lang', 'model : BackendModel', 'INSERT INTO `tbl_article_lang` (`article_id`, `lang_id`) VALUES (''3'', ''2'')', 2, '2014-06-11 17:32:50', '127.0.0.1'),
(280, 'tbl_article', 3, 'update Article', 'model : BackendModel<br /><b>Update</b><br />date : 2014-06-11 => 2014-12-31<br />sort_priority : 1 => 1', 'UPDATE `tbl_article` SET `date` = ''2014-12-31'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `article_id` =  ''3''', 2, '2014-06-11 17:38:06', '127.0.0.1'),
(281, 'tbl_article_lang', 5, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''3'', `lang_id` = ''1'' WHERE `article_lang_id` =  ''5''', 2, '2014-06-11 17:38:06', '127.0.0.1'),
(282, 'tbl_article_lang', 6, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''3'', `lang_id` = ''2'' WHERE `article_lang_id` =  ''6''', 2, '2014-06-11 17:38:06', '127.0.0.1'),
(283, 'tbl_article', 2, 'update Article', 'model : BackendModel<br /><b>Update</b><br />date : 2014-06-11 => 2014-10-23<br />sort_priority : 2 => 2', 'UPDATE `tbl_article` SET `date` = ''2014-10-23'', `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''2'' WHERE `article_id` =  ''2''', 2, '2014-06-11 17:38:41', '127.0.0.1'),
(284, 'tbl_article_lang', 3, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''2'', `lang_id` = ''1'' WHERE `article_lang_id` =  ''3''', 2, '2014-06-11 17:38:41', '127.0.0.1'),
(285, 'tbl_article_lang', 4, 'insert Article_lang', 'model : BackendModel', 'UPDATE `tbl_article_lang` SET `article_id` = ''2'', `lang_id` = ''2'' WHERE `article_lang_id` =  ''4''', 2, '2014-06-11 17:38:41', '127.0.0.1'),
(286, 'mother_table', 14, 'create table', 'model : StructureModel<br />parent_table_id : 9<br />table_name : Contactus_about<br />table_code : contactus_about<br />table_type : static<br />table_order : asc<br />table_newcontent : false<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 1', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''9'', ''Contactus_about'', ''contactus_about'', ''static'', ''asc'', ''false'', ''false'', '''', ''1'', ''1'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 17:46:13', '127.0.0.1'),
(287, 'mother_column', 46, 'create column', 'model : StructureModel<br />column_name : Map<br />column_code : map<br />column_main : true<br />column_field_type : url<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 14', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Map'', ''map'', ''true'', ''url'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''14'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 17:46:38', '127.0.0.1'),
(288, 'mother_column', 47, 'create column', 'model : StructureModel<br />column_name : Address<br />column_code : address<br />column_main : false<br />column_field_type : textarea<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 14', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Address'', ''address'', ''false'', ''textarea'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''1'', ''14'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 17:47:22', '127.0.0.1'),
(289, 'mother_table', 14, 'update table', 'model : StructureModel<br /><b>Update</b><br />parent_table_id : 9 => 0', 'UPDATE `mother_table` SET `parent_table_id` = ''0'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''14''', 2, '2014-06-11 17:48:16', '127.0.0.1'),
(290, 'mother_table', 13, 'update table', 'model : StructureModel<br /><b>Update</b><br />parent_table_id : 5 => 0', 'UPDATE `mother_table` SET `parent_table_id` = ''0'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''13''', 2, '2014-06-11 17:48:34', '127.0.0.1'),
(291, 'mother_table', 9, 'update table', 'model : StructureModel<br /><b>Update</b><br />parent_table_id : 0 => 14<br />sort_priority : 7 => 1', 'UPDATE `mother_table` SET `parent_table_id` = ''14'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-11 17:49:02', '127.0.0.1'),
(292, 'mother_table', 14, 'update table', 'model : StructureModel<br /><b>Update</b><br />sort_priority : 1 => 6', 'UPDATE `mother_table` SET `sort_priority` = ''6'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''14''', 2, '2014-06-11 17:49:24', '127.0.0.1'),
(293, 'tbl_contactus_about', 1, 'insert Contactus_about', 'model : BackendModel<br />address : 8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, Chonburi, Chon Buri, Thailand 20000\r\n\r\nMon - Sun: 08:00 - 17:00\r\n\r\nTel: 038-282-867<br />map : <br />recursive_id : 0', 'INSERT INTO `tbl_contactus_about` (`address`, `map`, `recursive_id`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, Chonburi, Chon Buri, Thailand 20000\\r\\n\\r\\nMon - Sun: 08:00 - 17:00\\r\\n\\r\\nTel: 038-282-867'', '''', 0, NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 17:49:48', '127.0.0.1'),
(294, 'tbl_contactus_about_lang', 1, 'insert Contactus_about_lang', 'model : BackendModel', 'INSERT INTO `tbl_contactus_about_lang` (`contactus_about_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-11 17:49:48', '127.0.0.1'),
(295, 'tbl_contactus_about_lang', 1, 'insert Contactus_about_lang', 'model : BackendModel', 'INSERT INTO `tbl_contactus_about_lang` (`contactus_about_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-11 17:49:48', '127.0.0.1'),
(296, 'mother_column', 46, 'delete column', 'model : StructureModel<br />column_id : 46<br />column_main : false => true<br />column_field_type : url => textarea', 'UPDATE `mother_column` SET `column_main` = ''true'', `column_field_type` = ''textarea'', `update_date` = NOW(), `update_by` = ''2'' WHERE `column_id` =  ''46''', 2, '2014-06-11 17:53:35', '127.0.0.1'),
(297, 'tbl_contactus_about', 1, 'update Contactus_about', 'model : BackendModel<br /><b>Update</b><br />map :  => <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7763.307215920085!2d100.9951672359552!3d13.371801888798618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d35c023fd2b1f%3A0x61317754beb0d74c!2z4Lie4Li44LiX4LiY4Liq4Lih4Liy4LiE4Lih4LiI4Li14LmI4Liu4LiH4LmB4Lir4LmI4LiH4Lib4Lij4Liw4LmA4LiX4Lio4LmE4LiX4LiiICjguIjguLXguYjguK7guIfguYDguIHguLLguLAp!5e0!3m2!1sth!2sth!4v1402483961739" width="600" height="450" frameborder="0" style="border:0"></iframe>', 'UPDATE `tbl_contactus_about` SET `map` = ''<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7763.307215920085!2d100.9951672359552!3d13.371801888798618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d35c023fd2b1f%3A0x61317754beb0d74c!2z4Lie4Li44LiX4LiY4Liq4Lih4Liy4LiE4Lih4LiI4Li14LmI4Liu4LiH4LmB4Lir4LmI4LiH4Lib4Lij4Liw4LmA4LiX4Lio4LmE4LiX4LiiICjguIjguLXguYjguK7guIfguYDguIHguLLguLAp!5e0!3m2!1sth!2sth!4v1402483961739\\" width=\\"600\\" height=\\"450\\" frameborder=\\"0\\" style=\\"border:0\\"></iframe>'', `update_date` = NOW(), `update_by` = ''2'' WHERE `contactus_about_id` =  ''1''', 2, '2014-06-11 17:54:09', '127.0.0.1'),
(298, 'tbl_contactus_about_lang', 1, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''1'' WHERE `contactus_about_lang_id` =  ''1''', 2, '2014-06-11 17:54:09', '127.0.0.1'),
(299, 'tbl_contactus_about_lang', 2, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''2'' WHERE `contactus_about_lang_id` =  ''2''', 2, '2014-06-11 17:54:09', '127.0.0.1'),
(300, 'tbl_contactus_about', 1, 'update Contactus_about', 'model : BackendModel<br /><b>Update</b><br />address : 8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, Chonburi, Chon Buri, Thailand 20000\r\n\r\nMon - Sun: 08:00 - 17:00\r\n\r\nTel: 038-282-867 =>       <p>8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, Chonburi, Chon Buri, Thailand 20000</p>\r\n                <p>Mon - Sun: 08:00 - 17:00</p>\r\n                <p>Tel: 038-282-867</p>', 'UPDATE `tbl_contactus_about` SET `address` = ''      <p>8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, Chonburi, Chon Buri, Thailand 20000</p>\\r\\n                <p>Mon - Sun: 08:00 - 17:00</p>\\r\\n                <p>Tel: 038-282-867</p>'', `update_date` = NOW(), `update_by` = ''2'' WHERE `contactus_about_id` =  ''1''', 2, '2014-06-11 17:58:39', '127.0.0.1'),
(301, 'tbl_contactus_about_lang', 1, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''1'' WHERE `contactus_about_lang_id` =  ''1''', 2, '2014-06-11 17:58:39', '127.0.0.1'),
(302, 'tbl_contactus_about_lang', 2, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''2'' WHERE `contactus_about_lang_id` =  ''2''', 2, '2014-06-11 17:58:39', '127.0.0.1'),
(303, 'mother_column', 48, 'create column', 'model : StructureModel<br />column_name : Facebook<br />column_code : facebook<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 3<br />table_id : 14', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Facebook'', ''facebook'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''3'', ''14'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 18:01:36', '127.0.0.1'),
(304, 'mother_column', 49, 'create column', 'model : StructureModel<br />column_name : Twitter<br />column_code : twitter<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 4<br />table_id : 14', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Twitter'', ''twitter'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''4'', ''14'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 18:03:03', '127.0.0.1'),
(305, 'tbl_contactus_about', 1, 'update Contactus_about', 'model : BackendModel<br /><b>Update</b><br />map : <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7763.307215920085!2d100.9951672359552!3d13.371801888798618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d35c023fd2b1f%3A0x61317754beb0d74c!2z4Lie4Li44LiX4LiY4Liq4Lih4Liy4LiE4Lih4LiI4Li14LmI4Liu4LiH4LmB4Lir4LmI4LiH4Lib4Lij4Liw4LmA4LiX4Lio4LmE4LiX4LiiICjguIjguLXguYjguK7guIfguYDguIHguLLguLAp!5e0!3m2!1sth!2sth!4v1402483961739" width="600" height="450" frameborder="0" style="border:0"></iframe> => <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7763.307215920085!2d100.9951672359552!3d13.371801888798618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d35c023fd2b1f%3A0x61317754beb0d74c!2z4Lie4Li44LiX4LiY4Liq4Lih4Liy4LiE4Lih4LiI4Li14LmI4Liu4LiH4LmB4Lir4LmI4LiH4Lib4Lij4Liw4LmA4LiX4Lio4LmE4LiX4LiiICjguIjguLXguYjguK7guIfguYDguIHguLLguLAp!5e0!3m2!1sth!2sth!4v1402483961739" width="913" height="424" frameborder="0" style="border:0"></iframe>', 'UPDATE `tbl_contactus_about` SET `map` = ''<iframe src=\\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7763.307215920085!2d100.9951672359552!3d13.371801888798618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d35c023fd2b1f%3A0x61317754beb0d74c!2z4Lie4Li44LiX4LiY4Liq4Lih4Liy4LiE4Lih4LiI4Li14LmI4Liu4LiH4LmB4Lir4LmI4LiH4Lib4Lij4Liw4LmA4LiX4Lio4LmE4LiX4LiiICjguIjguLXguYjguK7guIfguYDguIHguLLguLAp!5e0!3m2!1sth!2sth!4v1402483961739\\" width=\\"913\\" height=\\"424\\" frameborder=\\"0\\" style=\\"border:0\\"></iframe>'', `update_date` = NOW(), `update_by` = ''2'' WHERE `contactus_about_id` =  ''1''', 2, '2014-06-11 18:08:08', '127.0.0.1'),
(306, 'tbl_contactus_about_lang', 1, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''1'' WHERE `contactus_about_lang_id` =  ''1''', 2, '2014-06-11 18:08:08', '127.0.0.1'),
(307, 'tbl_contactus_about_lang', 2, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''2'' WHERE `contactus_about_lang_id` =  ''2''', 2, '2014-06-11 18:08:08', '127.0.0.1'),
(308, 'tbl_contactus_about', 1, 'update Contactus_about', 'model : BackendModel<br /><b>Update</b><br />facebook :  => https://www.facebook.com/<br />twitter :  => https://www.twitter.com/', 'UPDATE `tbl_contactus_about` SET `facebook` = ''https://www.facebook.com/'', `twitter` = ''https://www.twitter.com/'', `update_date` = NOW(), `update_by` = ''2'' WHERE `contactus_about_id` =  ''1''', 2, '2014-06-11 18:09:29', '127.0.0.1'),
(309, 'tbl_contactus_about_lang', 1, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''1'' WHERE `contactus_about_lang_id` =  ''1''', 2, '2014-06-11 18:09:29', '127.0.0.1'),
(310, 'tbl_contactus_about_lang', 2, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''2'' WHERE `contactus_about_lang_id` =  ''2''', 2, '2014-06-11 18:09:29', '127.0.0.1'),
(311, 'mother_column', 26, 'delete column', 'model : StructureModel<br />column_id : 26<br />column_name : Lastname => surname<br />column_code : lastname => surname', 'UPDATE `mother_column` SET `column_name` = ''surname'', `column_code` = ''surname'', `update_date` = NOW(), `update_by` = ''2'' WHERE `column_id` =  ''26''', 2, '2014-06-11 18:21:21', '127.0.0.1'),
(312, 'mother_table', 9, 'update table', 'model : StructureModel<br /><b>Update</b><br />table_newcontent : false => true', 'UPDATE `mother_table` SET `table_newcontent` = ''true'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-11 18:23:25', '127.0.0.1'),
(313, 'mother_table', 9, 'update table', 'model : StructureModel<br /><b>Update</b><br />parent_table_id : 14 => 0', 'UPDATE `mother_table` SET `parent_table_id` = ''0'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-11 18:27:25', '127.0.0.1'),
(314, 'mother_table', 13, 'move down table', 'model : StructureModel<br />table_id : 13<br /><b>Move Down Priority</b>', 'UPDATE `mother_table` SET `sort_priority` = 1, `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''13''', 2, '2014-06-11 18:28:37', '127.0.0.1'),
(315, 'mother_table', 9, 'move down table', 'model : StructureModel<br />table_id : 9<br /><b>Move Down Priority</b>', 'UPDATE `mother_table` SET `sort_priority` = 1, `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-11 18:28:42', '127.0.0.1'),
(316, 'mother_table', 9, 'move down table', 'model : StructureModel<br />table_id : 9<br /><b>Move Down Priority</b>', 'UPDATE `mother_table` SET `sort_priority` = 2, `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-11 18:28:52', '127.0.0.1'),
(317, 'mother_table', 9, 'update table', 'model : StructureModel<br /><b>Update</b><br />sort_priority : 2 => 5', 'UPDATE `mother_table` SET `sort_priority` = ''5'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-11 18:29:09', '127.0.0.1'),
(318, 'mother_table', 9, 'update table', 'model : StructureModel<br /><b>Update</b><br />table_newcontent : true => false', 'UPDATE `mother_table` SET `table_newcontent` = ''false'', `update_date` = NOW(), `update_by` = ''2'' WHERE `table_id` =  ''9''', 2, '2014-06-11 18:29:30', '127.0.0.1'),
(319, 'tbl_contactus_about', 1, 'update Contactus_about', 'model : BackendModel<br /><b>Update</b><br />address :       <p>8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, Chonburi, Chon Buri, Thailand 20000</p>\r\n                <p>Mon - Sun: 08:00 - 17:00</p>\r\n                <p>Tel: 038-282-867</p> =>  <p>พุทธสมาคมจี่ฮงแห่งประเทศไทย จี่ฮงเกาะ</p>\r\n                <p>8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, </p>\r\n                <p>Chonburi, Chon Buri, Thailand 20000</p>\r\n                <p>โทร : 038 282 867</p>', 'UPDATE `tbl_contactus_about` SET `address` = '' <p>พุทธสมาคมจี่ฮงแห่งประเทศไทย จี่ฮงเกาะ</p>\\r\\n                <p>8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, </p>\\r\\n                <p>Chonburi, Chon Buri, Thailand 20000</p>\\r\\n                <p>โทร : 038 282 867</p>'', `update_date` = NOW(), `update_by` = ''2'' WHERE `contactus_about_id` =  ''1''', 2, '2014-06-11 18:31:40', '127.0.0.1'),
(320, 'tbl_contactus_about_lang', 1, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''1'' WHERE `contactus_about_lang_id` =  ''1''', 2, '2014-06-11 18:31:40', '127.0.0.1'),
(321, 'tbl_contactus_about_lang', 2, 'insert Contactus_about_lang', 'model : BackendModel', 'UPDATE `tbl_contactus_about_lang` SET `contactus_about_id` = ''1'', `lang_id` = ''2'' WHERE `contactus_about_lang_id` =  ''2''', 2, '2014-06-11 18:31:40', '127.0.0.1'),
(322, 'mother_table', 15, 'create table', 'model : StructureModel<br />parent_table_id : 0<br />table_name : Other_link<br />table_code : other_link<br />table_type : dynamic<br />table_order : desc<br />table_newcontent : true<br />table_recursive : false<br />table_preview : <br />icon_id : 1<br />sort_priority : 9', 'INSERT INTO `mother_table` (`parent_table_id`, `table_name`, `table_code`, `table_type`, `table_order`, `table_newcontent`, `table_recursive`, `table_preview`, `icon_id`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''0'', ''Other_link'', ''other_link'', ''dynamic'', ''desc'', ''true'', ''false'', '''', ''1'', ''9'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 19:02:20', '127.0.0.1'),
(323, 'mother_column', 50, 'create column', 'model : StructureModel<br />column_name : Name<br />column_code : name<br />column_main : true<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 1<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 1<br />table_id : 15', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Name'', ''name'', ''true'', ''text'', ''0'', ''0'', ''1'', ''Disable'', '''', '''', ''1'', ''15'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 19:02:36', '127.0.0.1'),
(324, 'mother_column', 51, 'create column', 'model : StructureModel<br />column_name : Link<br />column_code : link<br />column_main : false<br />column_field_type : text<br />column_relation_table : 0<br />column_lang : 0<br />column_show_list : 0<br />searchable : Disable<br />column_option : <br />column_remark : <br />sort_priority : 2<br />table_id : 15', 'INSERT INTO `mother_column` (`column_name`, `column_code`, `column_main`, `column_field_type`, `column_relation_table`, `column_lang`, `column_show_list`, `searchable`, `column_option`, `column_remark`, `sort_priority`, `table_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (''Link'', ''link'', ''false'', ''text'', ''0'', ''0'', ''0'', ''Disable'', '''', '''', ''2'', ''15'', NOW(), ''2'', NOW(), ''2'')', 2, '2014-06-11 19:02:49', '127.0.0.1'),
(325, 'tbl_other_link', 1, 'insert Other_link', 'model : BackendModel<br />name : เต็กก่าซิมเตี้ยง<br />link : <br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_other_link` (`name`, `link`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''เต็กก่าซิมเตี้ยง'', '''', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 19:03:11', '127.0.0.1'),
(326, 'tbl_other_link_lang', 1, 'insert Other_link_lang', 'model : BackendModel', 'INSERT INTO `tbl_other_link_lang` (`other_link_id`, `lang_id`) VALUES (''1'', ''1'')', 2, '2014-06-11 19:03:11', '127.0.0.1'),
(327, 'tbl_other_link_lang', 1, 'insert Other_link_lang', 'model : BackendModel', 'INSERT INTO `tbl_other_link_lang` (`other_link_id`, `lang_id`) VALUES (''1'', ''2'')', 2, '2014-06-11 19:03:11', '127.0.0.1'),
(328, 'tbl_other_link', 2, 'insert Other_link', 'model : BackendModel<br />name : เต็กก่าซิมเตี้ยง<br />link : <br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_other_link` (`name`, `link`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''เต็กก่าซิมเตี้ยง'', '''', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 19:03:17', '127.0.0.1'),
(329, 'tbl_other_link_lang', 2, 'insert Other_link_lang', 'model : BackendModel', 'INSERT INTO `tbl_other_link_lang` (`other_link_id`, `lang_id`) VALUES (''2'', ''1'')', 2, '2014-06-11 19:03:17', '127.0.0.1'),
(330, 'tbl_other_link_lang', 2, 'insert Other_link_lang', 'model : BackendModel', 'INSERT INTO `tbl_other_link_lang` (`other_link_id`, `lang_id`) VALUES (''2'', ''2'')', 2, '2014-06-11 19:03:17', '127.0.0.1'),
(331, 'tbl_other_link', 3, 'insert Other_link', 'model : BackendModel<br />name : เต็กก่าซิมเตี้ยง<br />link : <br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_other_link` (`name`, `link`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''เต็กก่าซิมเตี้ยง'', '''', 0, ''1'', ''show'', NOW(), ''2'', NOW(), ''2'', ''1'')', 2, '2014-06-11 19:03:23', '127.0.0.1'),
(332, 'tbl_other_link_lang', 3, 'insert Other_link_lang', 'model : BackendModel', 'INSERT INTO `tbl_other_link_lang` (`other_link_id`, `lang_id`) VALUES (''3'', ''1'')', 2, '2014-06-11 19:03:23', '127.0.0.1'),
(333, 'tbl_other_link_lang', 3, 'insert Other_link_lang', 'model : BackendModel', 'INSERT INTO `tbl_other_link_lang` (`other_link_id`, `lang_id`) VALUES (''3'', ''2'')', 2, '2014-06-11 19:03:23', '127.0.0.1'),
(334, 'tbl_other_link', 3, 'update Other_link', 'model : BackendModel<br /><b>Update</b><br />link :  => https://www.facebook.com/<br />sort_priority : 1 => 1', 'UPDATE `tbl_other_link` SET `link` = ''https://www.facebook.com/'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''2'' WHERE `other_link_id` =  ''3''', 2, '2014-06-11 19:06:12', '127.0.0.1'),
(335, 'tbl_other_link_lang', 5, 'insert Other_link_lang', 'model : BackendModel', 'UPDATE `tbl_other_link_lang` SET `other_link_id` = ''3'', `lang_id` = ''1'' WHERE `other_link_lang_id` =  ''5''', 2, '2014-06-11 19:06:12', '127.0.0.1'),
(336, 'tbl_other_link_lang', 6, 'insert Other_link_lang', 'model : BackendModel', 'UPDATE `tbl_other_link_lang` SET `other_link_id` = ''3'', `lang_id` = ''2'' WHERE `other_link_lang_id` =  ''6''', 2, '2014-06-11 19:06:12', '127.0.0.1'),
(337, 'tbl_inside', 1, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : วิหารเต๋าโจ้ว => CheeGarden<br />image : /userfiles/images/1(3).png => /userfiles/images/inside/cheegarden/282121_10150301920424047_5019514_n.jpg<br />sort_priority : 6 => 1', 'UPDATE `tbl_inside` SET `name` = ''CheeGarden'', `image` = ''/userfiles/images/inside/cheegarden/282121_10150301920424047_5019514_n.jpg'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''1''', 1, '2014-06-30 14:09:46', '171.96.18.207'),
(338, 'tbl_inside_lang', 1, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''1'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''1''', 1, '2014-06-30 14:09:46', '171.96.18.207'),
(339, 'tbl_inside_lang', 2, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''1'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''2''', 1, '2014-06-30 14:09:46', '171.96.18.207'),
(340, 'tbl_inside', 1, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : CheeGarden => Chee Garden<br />sort_priority : 1 => 1', 'UPDATE `tbl_inside` SET `name` = ''Chee Garden'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''1''', 1, '2014-06-30 14:09:54', '171.96.18.207'),
(341, 'tbl_inside_lang', 1, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''1'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''1''', 1, '2014-06-30 14:09:54', '171.96.18.207'),
(342, 'tbl_inside_lang', 2, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''1'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''2''', 1, '2014-06-30 14:09:54', '171.96.18.207'),
(343, 'tbl_inside', 2, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : วิหารเต๋าโจ้ว => พลับพลาตรีดารา<br />image : /userfiles/images/1(3).png => /userfiles/images/inside/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/167505_10150124054374047_684171_n.jpg<br />sort_priority : 6 => 2', 'UPDATE `tbl_inside` SET `name` = ''พลับพลาตรีดารา'', `image` = ''/userfiles/images/inside/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/167505_10150124054374047_684171_n.jpg'', `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''2''', 1, '2014-06-30 14:10:56', '171.96.18.207'),
(344, 'tbl_inside_lang', 3, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''2'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''3''', 1, '2014-06-30 14:10:56', '171.96.18.207'),
(345, 'tbl_inside_lang', 4, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''2'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''4''', 1, '2014-06-30 14:10:56', '171.96.18.207'),
(346, 'tbl_inside', 1, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/inside/cheegarden/282121_10150301920424047_5019514_n.jpg => /userfiles/images/inside/cheegarden/CheeGarden_1.png<br />sort_priority : 1 => 1', 'UPDATE `tbl_inside` SET `image` = ''/userfiles/images/inside/cheegarden/CheeGarden_1.png'', `sort_priority` = ''1'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''1''', 1, '2014-06-30 14:13:15', '171.96.18.207'),
(347, 'tbl_inside_lang', 1, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''1'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''1''', 1, '2014-06-30 14:13:15', '171.96.18.207'),
(348, 'tbl_inside_lang', 2, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''1'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''2''', 1, '2014-06-30 14:13:15', '171.96.18.207'),
(349, 'tbl_inside', 2, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/inside/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/167505_10150124054374047_684171_n.jpg => /userfiles/images/inside/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2_1.png<br />sort_priority : 2 => 2', 'UPDATE `tbl_inside` SET `image` = ''/userfiles/images/inside/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2_1.png'', `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''2''', 1, '2014-06-30 14:31:14', '171.96.18.207'),
(350, 'tbl_inside_lang', 3, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''2'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''3''', 1, '2014-06-30 14:31:14', '171.96.18.207'),
(351, 'tbl_inside_lang', 4, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''2'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''4''', 1, '2014-06-30 14:31:14', '171.96.18.207'),
(352, 'tbl_inside', 2, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/inside/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0% => /userfiles/images/inside/%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2_1.png<br />sort_priority : 2 => 2', 'UPDATE `tbl_inside` SET `image` = ''/userfiles/images/inside/%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2_1.png'', `sort_priority` = ''2'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''2''', 1, '2014-06-30 14:31:49', '171.96.18.207'),
(353, 'tbl_inside_lang', 3, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''2'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''3''', 1, '2014-06-30 14:31:49', '171.96.18.207'),
(354, 'tbl_inside_lang', 4, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''2'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''4''', 1, '2014-06-30 14:31:49', '171.96.18.207');
INSERT INTO `log_action` (`action_id`, `table_name`, `content_id`, `action_name`, `action_detail`, `action_query`, `create_by`, `create_date`, `create_ip`) VALUES
(355, 'tbl_inside', 3, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : วิหารเต๋าโจ้ว => วิหารเง็กอ้วงไต่เทียงจุง<br />image : /userfiles/images/1(3).png => /userfiles/images/inside/%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg<br />sort_priority : 6 => 3', 'UPDATE `tbl_inside` SET `name` = ''วิหารเง็กอ้วงไต่เทียงจุง'', `image` = ''/userfiles/images/inside/%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg'', `sort_priority` = ''3'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''3''', 1, '2014-07-01 15:48:07', '115.87.71.101'),
(356, 'tbl_inside_lang', 5, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''3'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''5''', 1, '2014-07-01 15:48:10', '115.87.71.101'),
(357, 'tbl_inside_lang', 6, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''3'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''6''', 1, '2014-07-01 15:48:10', '115.87.71.101'),
(358, 'tbl_inside', 3, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/inside/%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0% => /userfiles/images/inside/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg<br />sort_priority : 3 => 3', 'UPDATE `tbl_inside` SET `image` = ''/userfiles/images/inside/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg'', `sort_priority` = ''3'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''3''', 1, '2014-07-01 15:48:32', '115.87.71.101'),
(359, 'tbl_inside_lang', 5, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''3'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''5''', 1, '2014-07-01 15:48:32', '115.87.71.101'),
(360, 'tbl_inside_lang', 6, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''3'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''6''', 1, '2014-07-01 15:48:32', '115.87.71.101'),
(361, 'tbl_inside', 3, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/inside/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%87%E0%B9%87%E0%B8%81%E0%B8%AD%E0%B9%89%E0%B8%A7%E0%B8%87%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0% => /userfiles/images/inside/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg<br />sort_priority : 3 => 3', 'UPDATE `tbl_inside` SET `image` = ''/userfiles/images/inside/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg'', `sort_priority` = ''3'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''3''', 1, '2014-07-01 15:48:46', '115.87.71.101'),
(362, 'tbl_inside_lang', 5, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''3'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''5''', 1, '2014-07-01 15:48:46', '115.87.71.101'),
(363, 'tbl_inside_lang', 6, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''3'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''6''', 1, '2014-07-01 15:48:46', '115.87.71.101'),
(364, 'tbl_inside', 6, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : วิหารเต๋าโจ้ว => วิหารไช่แปะแชกุง<br />image : /userfiles/images/1(3).png => /userfiles/images/inside/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg<br />sort_priority : 4 => 4', 'UPDATE `tbl_inside` SET `name` = ''วิหารไช่แปะแชกุง'', `image` = ''/userfiles/images/inside/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg'', `sort_priority` = ''4'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''6''', 1, '2014-07-01 15:51:03', '115.87.71.101'),
(365, 'tbl_inside_lang', 11, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''6'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''11''', 1, '2014-07-01 15:51:03', '115.87.71.101'),
(366, 'tbl_inside_lang', 12, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''6'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''12''', 1, '2014-07-01 15:51:03', '115.87.71.101'),
(367, 'tbl_inside', 4, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : วิหารเต๋าโจ้ว => วิหารไช่แปะแชกุง<br />image : /userfiles/images/1(3).png => /userfiles/images/inside/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg<br />sort_priority : 6 => 4', 'UPDATE `tbl_inside` SET `name` = ''วิหารไช่แปะแชกุง'', `image` = ''/userfiles/images/inside/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg'', `sort_priority` = ''4'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''4''', 1, '2014-07-01 15:51:29', '115.87.71.101'),
(368, 'tbl_inside_lang', 7, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''4'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''7''', 1, '2014-07-01 15:51:29', '115.87.71.101'),
(369, 'tbl_inside_lang', 8, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''4'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''8''', 1, '2014-07-01 15:51:29', '115.87.71.101'),
(370, 'tbl_inside', 5, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : วิหารเต๋าโจ้ว => วิหารซือจุง<br />image : /userfiles/images/1(3).png => /userfiles/images/inside/%E0%B8%8B%E0%B8%B7%E0%B8%AD%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%8B%E0%B8%B7%E0%B8%AD%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg<br />sort_priority : 6 => 5', 'UPDATE `tbl_inside` SET `name` = ''วิหารซือจุง'', `image` = ''/userfiles/images/inside/%E0%B8%8B%E0%B8%B7%E0%B8%AD%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%8B%E0%B8%B7%E0%B8%AD%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg'', `sort_priority` = ''5'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''5''', 1, '2014-07-01 15:55:49', '115.87.71.101'),
(371, 'tbl_inside_lang', 9, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''5'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''9''', 1, '2014-07-01 15:55:49', '115.87.71.101'),
(372, 'tbl_inside_lang', 10, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''5'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''10''', 1, '2014-07-01 15:55:49', '115.87.71.101'),
(373, 'tbl_inside', 6, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />sort_priority : 6 => 6', 'UPDATE `tbl_inside` SET `sort_priority` = ''6'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''6''', 1, '2014-07-01 15:56:00', '115.87.71.101'),
(374, 'tbl_inside', 6, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />name : วิหารไช่แปะแชกุง => วิหารเต๋าโจ้ว<br />image : /userfiles/images/inside/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg => /userfiles/images/inside/%E0%B9%80%E0%B8%95%E0%B9%8B%E0%B8%B2%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%80%E0%B8%95%E0%B9%8B%E0%B8%B2%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg<br />sort_priority : 6 => 6', 'UPDATE `tbl_inside` SET `name` = ''วิหารเต๋าโจ้ว'', `image` = ''/userfiles/images/inside/%E0%B9%80%E0%B8%95%E0%B9%8B%E0%B8%B2%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%80%E0%B8%95%E0%B9%8B%E0%B8%B2%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg'', `sort_priority` = ''6'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''6''', 1, '2014-07-01 15:58:11', '115.87.71.101'),
(375, 'tbl_inside_lang', 11, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''6'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''11''', 1, '2014-07-01 15:58:11', '115.87.71.101'),
(376, 'tbl_inside_lang', 12, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''6'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''12''', 1, '2014-07-01 15:58:11', '115.87.71.101'),
(377, 'tbl_inside', 7, 'insert Inside', 'model : BackendModel<br />name : วิหารไต่เสี่ยฮุกโจ้ว<br />image : /userfiles/images/inside/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg<br />recursive_id : 0<br />sort_priority : 7<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารไต่เสี่ยฮุกโจ้ว'', ''/userfiles/images/inside/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg'', 0, ''7'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2014-07-01 15:59:59', '115.87.71.101'),
(378, 'tbl_inside_lang', 7, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''7'', ''1'')', 1, '2014-07-01 15:59:59', '115.87.71.101'),
(379, 'tbl_inside_lang', 7, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''7'', ''2'')', 1, '2014-07-01 15:59:59', '115.87.71.101'),
(380, 'tbl_inside', 7, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/inside/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0% => /userfiles/images/inside/%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg<br />sort_priority : 7 => 7', 'UPDATE `tbl_inside` SET `image` = ''/userfiles/images/inside/%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg'', `sort_priority` = ''7'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''7''', 1, '2014-07-01 16:00:12', '115.87.71.101'),
(381, 'tbl_inside_lang', 13, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''7'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''13''', 1, '2014-07-01 16:00:12', '115.87.71.101'),
(382, 'tbl_inside_lang', 14, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''7'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''14''', 1, '2014-07-01 16:00:12', '115.87.71.101'),
(383, 'tbl_inside', 8, 'insert Inside', 'model : BackendModel<br />name : วิหารน่ำปักแชกุง<br />image : /userfiles/images/inside/%E0%B8%99%E0%B9%88%E0%B8%B3%E0%B8%9B%E0%B8%B1%E0%B8%81%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B8%99%E0%B9%88%E0%B8%B3%E0%B8%9B%E0%B8%B1%E0%B8%81%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg<br />recursive_id : 0<br />sort_priority : 8<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารน่ำปักแชกุง'', ''/userfiles/images/inside/%E0%B8%99%E0%B9%88%E0%B8%B3%E0%B8%9B%E0%B8%B1%E0%B8%81%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B8%99%E0%B9%88%E0%B8%B3%E0%B8%9B%E0%B8%B1%E0%B8%81%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg'', 0, ''8'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2014-07-01 16:01:46', '115.87.71.101'),
(384, 'tbl_inside_lang', 8, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''8'', ''1'')', 1, '2014-07-01 16:01:46', '115.87.71.101'),
(385, 'tbl_inside_lang', 8, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''8'', ''2'')', 1, '2014-07-01 16:01:46', '115.87.71.101'),
(386, 'tbl_inside', 9, 'insert Inside', 'model : BackendModel<br />name : วิหารพระกษิติครรภ์<br />image : /userfiles/images/inside/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C_1.jpg<br />recursive_id : 0<br />sort_priority : 1<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารพระกษิติครรภ์'', ''/userfiles/images/inside/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C_1.jpg'', 0, ''1'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2014-07-01 16:03:10', '115.87.71.101'),
(387, 'tbl_inside_lang', 9, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''9'', ''1'')', 1, '2014-07-01 16:03:10', '115.87.71.101'),
(388, 'tbl_inside_lang', 9, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''9'', ''2'')', 1, '2014-07-01 16:03:10', '115.87.71.101'),
(389, 'tbl_inside', 9, 'update Inside', 'model : BackendModel<br /><b>Update</b><br />image : /userfiles/images/inside/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0% => /userfiles/images/inside/%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C_1.jpg<br />sort_priority : 1 => 9', 'UPDATE `tbl_inside` SET `image` = ''/userfiles/images/inside/%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C_1.jpg'', `sort_priority` = ''9'', `update_date` = NOW(), `update_by` = ''1'' WHERE `inside_id` =  ''9''', 1, '2014-07-01 16:03:25', '115.87.71.101'),
(390, 'tbl_inside_lang', 17, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''9'', `lang_id` = ''1'' WHERE `inside_lang_id` =  ''17''', 1, '2014-07-01 16:03:25', '115.87.71.101'),
(391, 'tbl_inside_lang', 18, 'insert Inside_lang', 'model : BackendModel', 'UPDATE `tbl_inside_lang` SET `inside_id` = ''9'', `lang_id` = ''2'' WHERE `inside_lang_id` =  ''18''', 1, '2014-07-01 16:03:25', '115.87.71.101'),
(392, 'tbl_inside', 10, 'insert Inside', 'model : BackendModel<br />name : วิหารพระพุทธ-พุทธวิหาร<br />image : /userfiles/images/inside/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3_1.jpg<br />recursive_id : 0<br />sort_priority : 10<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารพระพุทธ-พุทธวิหาร'', ''/userfiles/images/inside/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3_1.jpg'', 0, ''10'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2014-07-01 16:04:57', '115.87.71.101'),
(393, 'tbl_inside_lang', 10, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''10'', ''1'')', 1, '2014-07-01 16:04:57', '115.87.71.101'),
(394, 'tbl_inside_lang', 10, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''10'', ''2'')', 1, '2014-07-01 16:04:57', '115.87.71.101'),
(395, 'tbl_inside', 11, 'insert Inside', 'model : BackendModel<br />name : วิหารพระอวโลกิเตศวรโพธิสัตว์<br />image : /userfiles/images/inside/%E0%B8%AD%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81%E0%B8%B4%E0%B9%80%E0%B8%95%E0%B8%A8%E0%B8%A7%E0%B8%A3/%E0%B8%AD%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81%E0%B8%B4%E0%B9%80%E0%B8%95%E0%B8%A8%E0%B8%A7%E0%B8%A3_1.jpg<br />recursive_id : 0<br />sort_priority : 11<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''วิหารพระอวโลกิเตศวรโพธิสัตว์'', ''/userfiles/images/inside/%E0%B8%AD%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81%E0%B8%B4%E0%B9%80%E0%B8%95%E0%B8%A8%E0%B8%A7%E0%B8%A3/%E0%B8%AD%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81%E0%B8%B4%E0%B9%80%E0%B8%95%E0%B8%A8%E0%B8%A7%E0%B8%A3_1.jpg'', 0, ''11'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2014-07-01 16:07:10', '115.87.71.101'),
(396, 'tbl_inside_lang', 11, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''11'', ''1'')', 1, '2014-07-01 16:07:10', '115.87.71.101'),
(397, 'tbl_inside_lang', 11, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''11'', ''2'')', 1, '2014-07-01 16:07:10', '115.87.71.101'),
(398, 'tbl_inside', 12, 'insert Inside', 'model : BackendModel<br />name : พุทธรังษีรัตนเจดีย์<br />image : /userfiles/images/inside/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A3%E0%B8%B1%E0%B8%87%E0%B8%A9%E0%B8%B5/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A3%E0%B8%B1%E0%B8%87%E0%B8%A9%E0%B8%B5_1.jpg<br />recursive_id : 0<br />sort_priority : 12<br />enable_status : show', 'INSERT INTO `tbl_inside` (`name`, `image`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `mother_shop_id`) VALUES (''พุทธรังษีรัตนเจดีย์'', ''/userfiles/images/inside/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A3%E0%B8%B1%E0%B8%87%E0%B8%A9%E0%B8%B5/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A3%E0%B8%B1%E0%B8%87%E0%B8%A9%E0%B8%B5_1.jpg'', 0, ''12'', ''show'', NOW(), ''1'', NOW(), ''1'', ''1'')', 1, '2014-07-01 16:21:25', '115.87.71.101'),
(399, 'tbl_inside_lang', 12, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''12'', ''1'')', 1, '2014-07-01 16:21:25', '115.87.71.101'),
(400, 'tbl_inside_lang', 12, 'insert Inside_lang', 'model : BackendModel', 'INSERT INTO `tbl_inside_lang` (`inside_id`, `lang_id`) VALUES (''12'', ''2'')', 1, '2014-07-01 16:21:25', '115.87.71.101');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(13);

-- --------------------------------------------------------

--
-- Table structure for table `mother_column`
--

CREATE TABLE IF NOT EXISTS `mother_column` (
  `column_id` int(11) unsigned NOT NULL auto_increment,
  `table_id` int(11) NOT NULL,
  `column_name` varchar(50) NOT NULL,
  `column_code` varchar(50) NOT NULL,
  `column_main` enum('true','false') NOT NULL default 'false',
  `column_field_type` varchar(50) NOT NULL,
  `column_lang` tinyint(4) NOT NULL,
  `column_show_list` tinyint(4) NOT NULL,
  `column_option` varchar(255) NOT NULL,
  `column_relation_table` int(11) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `searchable` enum('Disable','Normal Search','Advance Search') NOT NULL,
  `column_remark` varchar(255) NOT NULL,
  PRIMARY KEY  (`column_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `mother_column`
--

INSERT INTO `mother_column` (`column_id`, `table_id`, `column_name`, `column_code`, `column_main`, `column_field_type`, `column_lang`, `column_show_list`, `column_option`, `column_relation_table`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`, `searchable`, `column_remark`) VALUES
(5, 4, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-06 14:57:12', 2, '2014-06-06 14:57:34', 2, 'Disable', ''),
(4, 3, 'a', 'a', 'true', 'text', 0, 1, '', 0, 1, '2013-11-27 09:44:19', 1, '2013-11-27 09:44:19', 1, 'Disable', ''),
(6, 4, 'Image', 'image', 'false', 'image', 0, 0, '', 0, 3, '2014-06-06 14:57:51', 2, '2014-06-06 14:57:51', 2, 'Disable', ''),
(45, 13, 'Image', 'image', 'false', 'image', 0, 0, '', 0, 3, '2014-06-11 15:42:06', 2, '2014-06-11 15:42:06', 2, 'Disable', ''),
(44, 13, 'Title', 'title', 'false', 'text', 0, 0, '', 0, 2, '2014-06-11 15:41:50', 2, '2014-06-11 15:41:50', 2, 'Disable', ''),
(9, 5, 'Detail', 'detail', 'false', 'richtext', 0, 0, '', 0, 1, '2014-06-09 11:27:32', 2, '2014-06-09 11:27:32', 2, 'Disable', ''),
(43, 13, 'Date', 'date', 'true', 'date', 0, 1, '', 0, 1, '2014-06-11 15:41:31', 2, '2014-06-11 15:41:31', 2, 'Disable', ''),
(11, 6, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-09 11:29:42', 2, '2014-06-09 11:29:42', 2, 'Disable', ''),
(12, 6, 'Image', 'image', 'false', 'image', 0, 0, '', 0, 2, '2014-06-09 11:29:56', 2, '2014-06-09 11:29:56', 2, 'Disable', ''),
(13, 7, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-09 11:32:27', 2, '2014-06-09 11:32:27', 2, 'Disable', ''),
(14, 7, 'Title', 'title', 'false', 'text', 0, 0, '', 0, 2, '2014-06-09 11:32:42', 2, '2014-06-09 11:32:42', 2, 'Disable', ''),
(15, 7, 'Detail', 'detail', 'false', 'richtext', 0, 0, '', 0, 3, '2014-06-09 11:32:59', 2, '2014-06-09 11:32:59', 2, 'Disable', ''),
(16, 7, 'Date', 'date', 'false', 'date', 0, 0, '', 0, 4, '2014-06-09 11:33:23', 2, '2014-06-09 11:33:23', 2, 'Disable', ''),
(17, 7, 'Writer', 'writer', 'false', 'text', 0, 0, '', 0, 5, '2014-06-09 11:33:46', 2, '2014-06-09 11:33:46', 2, 'Disable', ''),
(18, 7, 'Image', 'image', 'false', 'image', 0, 0, '', 0, 6, '2014-06-09 11:34:01', 2, '2014-06-09 11:34:01', 2, 'Disable', ''),
(19, 8, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-09 11:35:55', 2, '2014-06-09 11:35:55', 2, 'Disable', ''),
(20, 8, 'Title', 'title', 'false', 'text', 0, 0, '', 0, 2, '2014-06-09 11:36:13', 2, '2014-06-09 11:36:13', 2, 'Disable', ''),
(21, 8, 'Date', 'date', 'false', 'date', 0, 0, '', 0, 3, '2014-06-09 11:36:50', 2, '2014-06-09 11:36:50', 2, 'Disable', ''),
(22, 8, 'Time', 'time', 'false', 'text', 0, 0, '', 0, 4, '2014-06-09 11:37:14', 2, '2014-06-09 11:37:14', 2, 'Disable', ''),
(23, 8, 'Detail', 'detail', 'false', 'richtext', 0, 0, '', 0, 5, '2014-06-09 11:37:33', 2, '2014-06-09 11:37:33', 2, 'Disable', ''),
(37, 11, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-10 13:55:55', 2, '2014-06-10 13:55:55', 2, 'Disable', ''),
(25, 9, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-09 12:25:37', 2, '2014-06-09 12:25:37', 2, 'Disable', ''),
(26, 9, 'surname', 'surname', 'false', 'text', 0, 0, '', 0, 2, '2014-06-09 12:25:52', 2, '2014-06-11 18:21:21', 2, 'Disable', ''),
(27, 9, 'Tel', 'tel', 'false', 'text', 0, 0, '', 0, 3, '2014-06-09 12:26:07', 2, '2014-06-09 12:26:07', 2, 'Disable', ''),
(28, 9, 'Email', 'email', 'false', 'text', 0, 0, '', 0, 4, '2014-06-09 12:26:20', 2, '2014-06-09 12:26:20', 2, 'Disable', ''),
(29, 9, 'Message', 'message', 'false', 'textarea', 0, 0, '', 0, 5, '2014-06-09 12:26:46', 2, '2014-06-09 12:32:24', 2, 'Disable', ''),
(40, 8, 'Image', 'image', 'false', 'image', 0, 0, '', 0, 6, '2014-06-10 13:58:09', 2, '2014-06-10 13:58:09', 2, 'Disable', ''),
(31, 4, 'Link', 'link', 'false', 'text', 0, 0, '', 0, 2, '2014-06-09 13:59:33', 2, '2014-06-09 13:59:33', 2, 'Disable', ''),
(32, 10, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-09 14:50:14', 2, '2014-06-09 14:50:14', 2, 'Disable', ''),
(33, 10, 'Time', 'time', 'false', 'text', 0, 0, '', 0, 3, '2014-06-09 14:50:36', 2, '2014-06-09 14:50:36', 2, 'Disable', ''),
(34, 10, 'Date', 'date', 'false', 'date', 0, 0, '', 0, 2, '2014-06-09 14:51:07', 2, '2014-06-09 14:51:07', 2, 'Disable', ''),
(35, 10, 'Title', 'title', 'false', 'text', 0, 0, '', 0, 4, '2014-06-09 14:51:26', 2, '2014-06-09 14:51:26', 2, 'Disable', ''),
(36, 10, 'Detail', 'detail', 'false', 'richtext', 0, 0, '', 0, 5, '2014-06-09 14:51:59', 2, '2014-06-09 14:51:59', 2, 'Disable', ''),
(38, 11, 'Embed', 'embed', 'false', 'url', 0, 0, '', 0, 2, '2014-06-10 13:56:13', 2, '2014-06-10 14:27:44', 2, 'Disable', ''),
(39, 11, 'Date', 'date', 'false', 'date', 0, 0, '', 0, 3, '2014-06-10 13:57:20', 2, '2014-06-10 13:57:20', 2, 'Disable', ''),
(41, 12, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-10 14:48:24', 2, '2014-06-10 14:48:24', 2, 'Disable', ''),
(42, 12, 'Image', 'image', 'false', 'image', 0, 0, '', 0, 2, '2014-06-10 14:48:43', 2, '2014-06-10 14:48:43', 2, 'Disable', ''),
(46, 14, 'Map', 'map', 'true', 'textarea', 0, 1, '', 0, 2, '2014-06-11 17:46:38', 2, '2014-06-11 17:53:35', 2, 'Disable', ''),
(47, 14, 'Address', 'address', 'false', 'textarea', 0, 0, '', 0, 1, '2014-06-11 17:47:22', 2, '2014-06-11 17:47:22', 2, 'Disable', ''),
(48, 14, 'Facebook', 'facebook', 'false', 'text', 0, 0, '', 0, 3, '2014-06-11 18:01:36', 2, '2014-06-11 18:01:36', 2, 'Disable', ''),
(49, 14, 'Twitter', 'twitter', 'false', 'text', 0, 0, '', 0, 4, '2014-06-11 18:03:03', 2, '2014-06-11 18:03:03', 2, 'Disable', ''),
(50, 15, 'Name', 'name', 'true', 'text', 0, 1, '', 0, 1, '2014-06-11 19:02:36', 2, '2014-06-11 19:02:36', 2, 'Disable', ''),
(51, 15, 'Link', 'link', 'false', 'text', 0, 0, '', 0, 2, '2014-06-11 19:02:49', 2, '2014-06-11 19:02:49', 2, 'Disable', '');

-- --------------------------------------------------------

--
-- Table structure for table `mother_config`
--

CREATE TABLE IF NOT EXISTS `mother_config` (
  `config_id` int(11) unsigned NOT NULL auto_increment,
  `config_name` varchar(50) NOT NULL,
  `config_code` varchar(50) NOT NULL,
  `config_type` enum('content','admin') NOT NULL default 'content',
  `config_field_type` varchar(50) NOT NULL,
  `config_value` text NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `config_group_id` int(11) NOT NULL default '1',
  PRIMARY KEY  (`config_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `mother_config`
--

INSERT INTO `mother_config` (`config_id`, `config_name`, `config_code`, `config_type`, `config_field_type`, `config_value`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`, `config_group_id`) VALUES
(1, 'Backend Title', 'site_title', 'admin', 'text', 'Backend', 1, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 1),
(2, 'Navigate Color', 'navigate_color', 'admin', 'color', '#454545', 2, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 1),
(3, 'Navigate Shadow Color', 'navigate_color_shadow', 'admin', 'color', '#1f1f1f', 3, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 1),
(4, 'Navigate Color Active', 'navigate_color_active', 'admin', 'color', '#6b6b6b', 4, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 1),
(5, 'Website Title', 'website_title', 'admin', 'text', '', 1, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 2),
(6, 'Favicon', 'favicon', 'admin', 'file', '', 2, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 2),
(7, 'Meta Keywords', 'meta_keywords', 'admin', 'textarea', '', 3, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 2),
(8, 'Meta Description', 'meta_description', 'admin', 'textarea', '', 4, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 2),
(9, 'Email Account', 'email_account', 'admin', 'text', 'noreply@condolette.com', 1, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 3),
(10, 'Password', 'password', 'admin', 'text', '3Ba4S', 2, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 3),
(11, 'SMTP', 'smtp', 'admin', 'text', 'mail.mintedimages.com', 3, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 3),
(12, 'Port', 'port', 'admin', 'text', '', 4, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 3),
(13, 'To', 'to', 'admin', 'textarea', '', 5, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 3),
(14, 'CC', 'cc', 'admin', 'textarea', '', 6, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 3),
(15, 'BCC', 'bcc', 'admin', 'textarea', '', 7, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 3),
(16, 'Google Analytic', 'google_analytic', 'admin', 'textarea', '', 1, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 4),
(17, 'Google Remarketing', 'google_remarketing', 'admin', 'textarea', '', 2, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `mother_config_group`
--

CREATE TABLE IF NOT EXISTS `mother_config_group` (
  `config_group_id` int(11) unsigned NOT NULL auto_increment,
  `config_group_name` varchar(50) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY  (`config_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mother_config_group`
--

INSERT INTO `mother_config_group` (`config_group_id`, `config_group_name`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Backend Setting', 1, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(2, 'Frontend Setting', 2, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(3, 'Email Setting', 3, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(4, 'Statistics Setting', 4, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mother_icon`
--

CREATE TABLE IF NOT EXISTS `mother_icon` (
  `icon_id` int(11) unsigned NOT NULL auto_increment,
  `icon_name` varchar(50) NOT NULL,
  `icon_image` varchar(255) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY  (`icon_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `mother_icon`
--

INSERT INTO `mother_icon` (`icon_id`, `icon_name`, `icon_image`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Concept', '/userfiles/images/admin_icon/concept.png', 1, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(2, 'News', '/userfiles/images/admin_icon/news.png', 2, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(3, 'Contact', '/userfiles/images/admin_icon/contact.png', 3, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(4, 'Facility', '/userfiles/images/admin_icon/facility.png', 4, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(5, 'Floor Plan', '/userfiles/images/admin_icon/floorplan.png', 5, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(6, 'Gallery', '/userfiles/images/admin_icon/gallery.png', 6, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(7, 'Register', '/userfiles/images/admin_icon/register.png', 7, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0),
(8, 'Room type', '/userfiles/images/admin_icon/roomtype.png', 8, '2014-06-03 17:26:34', 0, '2014-06-03 17:26:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mother_lang`
--

CREATE TABLE IF NOT EXISTS `mother_lang` (
  `lang_id` int(11) unsigned NOT NULL auto_increment,
  `lang_name` varchar(50) NOT NULL,
  `lang_code` varchar(10) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY  (`lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mother_lang`
--

INSERT INTO `mother_lang` (`lang_id`, `lang_name`, `lang_code`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Thai', 'th', 1, '2013-11-21 15:50:33', 0, '2013-11-21 15:50:33', 0),
(2, 'English', 'english', 2, '2013-11-27 09:49:48', 1, '2013-11-27 09:49:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mother_permission`
--

CREATE TABLE IF NOT EXISTS `mother_permission` (
  `permission_id` int(11) unsigned NOT NULL auto_increment,
  `user_group_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `table_id` varchar(50) NOT NULL,
  `view` enum('true','false') NOT NULL default 'false',
  `edit` enum('true','false') NOT NULL default 'false',
  PRIMARY KEY  (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mother_shop`
--

CREATE TABLE IF NOT EXISTS `mother_shop` (
  `shop_id` int(11) unsigned NOT NULL auto_increment,
  `shop_name` varchar(50) NOT NULL,
  `shop_code` varchar(50) NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY  (`shop_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mother_shop`
--

INSERT INTO `mother_shop` (`shop_id`, `shop_name`, `shop_code`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Main', 'main', 1, '2013-11-21 15:50:33', 0, '2013-11-21 15:50:33', 0),
(2, 'Secornd', 'Secornd', 2, '2013-11-27 09:49:18', 1, '2013-11-27 09:49:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mother_table`
--

CREATE TABLE IF NOT EXISTS `mother_table` (
  `table_id` int(11) unsigned NOT NULL auto_increment,
  `parent_table_id` int(11) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `table_code` varchar(255) NOT NULL,
  `table_type` enum('static','dynamic') NOT NULL,
  `sort_priority` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `icon_id` int(11) NOT NULL default '0',
  `table_order` enum('asc','desc') NOT NULL default 'asc',
  `table_newcontent` enum('true','false') NOT NULL default 'true',
  `table_preview` text NOT NULL,
  `table_recursive` enum('true','false') NOT NULL default 'false',
  PRIMARY KEY  (`table_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `mother_table`
--

INSERT INTO `mother_table` (`table_id`, `parent_table_id`, `table_name`, `table_code`, `table_type`, `sort_priority`, `create_date`, `create_by`, `update_date`, `update_by`, `icon_id`, `table_order`, `table_newcontent`, `table_preview`, `table_recursive`) VALUES
(5, 0, 'About us', 'about', 'static', 1, '2014-06-09 11:24:51', 2, '2014-06-11 15:32:31', 2, 1, 'asc', 'true', '', 'false'),
(3, 2, 'Sub Content', 'sub_content', 'dynamic', 1, '2013-11-27 09:32:13', 1, '2013-11-27 09:43:57', 1, 0, 'asc', 'true', '', 'false'),
(4, 0, 'Home Slide', 'home_slide', 'dynamic', -1, '2014-06-06 14:46:23', 2, '2014-06-06 14:46:23', 2, 1, 'asc', 'true', '', 'false'),
(6, 0, 'Inside', 'inside', 'dynamic', 2, '2014-06-09 11:29:26', 2, '2014-06-09 11:29:26', 2, 1, 'desc', 'true', '', 'false'),
(7, 0, 'Article', 'article', 'dynamic', 3, '2014-06-09 11:32:02', 2, '2014-06-09 11:35:13', 2, 1, 'desc', 'true', '', 'false'),
(8, 0, 'News', 'news', 'dynamic', 4, '2014-06-09 11:35:37', 2, '2014-06-09 11:35:37', 2, 1, 'desc', 'true', '', 'false'),
(9, 0, 'Contact us', 'contact', 'dynamic', 5, '2014-06-09 11:38:34', 2, '2014-06-11 18:29:30', 2, 1, 'desc', 'false', '', 'false'),
(10, 0, 'Calendar', 'calendar', 'dynamic', 8, '2014-06-09 14:49:18', 2, '2014-06-09 14:52:18', 2, 1, 'desc', 'true', '', 'false'),
(11, 0, 'Vdo', 'vdo', 'dynamic', 7, '2014-06-10 13:54:29', 2, '2014-06-10 14:04:46', 2, 1, 'desc', 'true', '', 'false'),
(12, 8, 'Gallery_news', 'gallery_news', 'dynamic', 1, '2014-06-10 14:46:29', 2, '2014-06-10 14:46:29', 2, 1, 'desc', 'true', '', 'false'),
(13, 0, 'About_list', 'about_list', 'dynamic', 0, '2014-06-11 15:40:59', 2, '2014-06-11 18:28:37', 2, 1, 'desc', 'true', '', 'false'),
(15, 0, 'Other_link', 'other_link', 'dynamic', 9, '2014-06-11 19:02:20', 2, '2014-06-11 19:02:20', 2, 1, 'desc', 'true', '', 'false'),
(14, 0, 'Contactus_about', 'contactus_about', 'static', 6, '2014-06-11 17:46:13', 2, '2014-06-11 17:49:24', 2, 1, 'asc', 'false', '', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `mother_user`
--

CREATE TABLE IF NOT EXISTS `mother_user` (
  `user_id` int(11) unsigned NOT NULL auto_increment,
  `user_group_id` int(11) NOT NULL,
  `user_login_name` varchar(50) NOT NULL,
  `user_login_password` varchar(50) NOT NULL,
  `enable_status` enum('enable','disable') NOT NULL default 'enable',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mother_user`
--

INSERT INTO `mother_user` (`user_id`, `user_group_id`, `user_login_name`, `user_login_password`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 0, 'developer', '1a460e698f5d598976b3958c297eda1a', 'enable', '2013-11-21 15:50:32', 0, '2013-11-21 15:50:32', 0),
(2, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'enable', '2014-06-03 17:27:09', 1, '2014-06-03 17:27:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mother_user_group`
--

CREATE TABLE IF NOT EXISTS `mother_user_group` (
  `user_group_id` int(11) unsigned NOT NULL auto_increment,
  `user_group_name` varchar(50) NOT NULL,
  `user_group_permission` longtext NOT NULL,
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY  (`user_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about`
--

CREATE TABLE IF NOT EXISTS `tbl_about` (
  `about_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `detail` text NOT NULL,
  PRIMARY KEY  (`about_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_about`
--

INSERT INTO `tbl_about` (`about_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `detail`) VALUES
(1, 1, 0, 0, 0, 'show', '2014-06-11 15:34:57', 2, '2014-06-11 15:34:57', 2, '<p style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/img(1).png" style="width: 813px; height: 453px;" /></p>\r\n<p>\r\n	พุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ | 紫峰閣) ก่อตั้งเมื่อปี พ.ศ. 2499 CHEE HONG BUDDHIST ASSOCIATION OF THAILAND Founded in B.E.2499 ตั้งอยู่ที่ หมู่ 9 ถ.สุขประยูร ต.บ้านสวน อ.เมืองชลบุรี ในราวๆ ปี พ.ศ.2492 ได้มี เศรษฐีชาวจีนผู้หนึ่งซึ่งมีภูมิลำเนาอยู่ในจังหวัดพระนครได้เที่ยวเสาะแสวงหาที่ดินที่มีทำเลเหมาะเพื่อจัดสร้าง</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_about_lang` (
  `about_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `about_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`about_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_about_lang`
--

INSERT INTO `tbl_about_lang` (`about_lang_id`, `about_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about_list`
--

CREATE TABLE IF NOT EXISTS `tbl_about_list` (
  `about_list_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY  (`about_list_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_about_list`
--

INSERT INTO `tbl_about_list` (`about_list_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `date`, `title`, `image`) VALUES
(1, 1, 1, 0, 2, 'show', '2014-06-11 15:43:11', 2, '2014-06-11 15:43:11', 2, '2014-06-01', 'สมเด็จพระอริยวงศาคตญาณ สมเด็จพระสังฆราชสกลมหาสังฆปริณายก (อยู่ญาโณทยมหาเถร) วัดสระเกศราชวรมหาวิหาร สมเด็จพระสังฆราชพระองค์ที่ 15 แห่งกรุงรัตนโกสินทร์เสด็จเป็นองค์ ประธานในงานเปิด พุทธวิหารพุทธสมาคมจี่ฮง แห่งประเทศไทย พร้อมทั้งทรงประทานพระฉายาลักษณ์ แด่พุท', '/userfiles/images/1(2).png'),
(2, 1, 1, 0, 1, 'show', '2014-06-11 15:43:37', 2, '2014-06-11 15:43:37', 2, '2014-06-11', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '/userfiles/images/2.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_about_list_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_about_list_lang` (
  `about_list_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `about_list_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`about_list_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_about_list_lang`
--

INSERT INTO `tbl_about_list_lang` (`about_list_lang_id`, `about_list_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article`
--

CREATE TABLE IF NOT EXISTS `tbl_article` (
  `article_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `date` date NOT NULL,
  `writer` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY  (`article_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_article`
--

INSERT INTO `tbl_article` (`article_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `title`, `detail`, `date`, `writer`, `image`) VALUES
(1, 1, 0, 0, 3, 'show', '2014-06-11 17:30:03', 2, '2014-06-11 17:31:31', 2, 'พิธีวางศิลาฤกษ์มหาเจดีย์', 'เมื่อวันเสาร์ที่ 8 ธันวาคม นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี  โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ', '<h4 style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/1(4).png" style="width: 891px; height: 346px;" /></h4>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\r\n<br />\r\n<br />\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\r\n', '2014-06-11', 'Administrator ', '/userfiles/images/2(1).png'),
(2, 1, 0, 0, 2, 'show', '2014-06-11 17:32:10', 2, '2014-06-11 17:38:41', 2, 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '<h4 style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/1(4).png" style="width: 891px; height: 346px;" /></h4>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\r\n<br />\r\n<br />\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\r\n', '2014-10-23', 'Administrator', '/userfiles/images/2(1).png'),
(3, 1, 0, 0, 1, 'show', '2014-06-11 17:32:50', 2, '2014-06-11 17:38:06', 2, 'วิหารเต๋าโจ้ว', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '<h4 style="text-align: center;">\r\n	<img alt="" src="/userfiles/images/1(4).png" style="width: 891px; height: 346px;" /></h4>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เขียนโดย Administrator &nbsp;&nbsp;</p>\r\n<br />\r\n<br />\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป<br />\r\n	<br />\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้น ที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็นสิริมงคลและเจริญ รุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ- เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชาเพื่อความ เป็น สิริมงคลและเจริญรุ่งเรื่องสืบไป&nbsp;</p>\r\n', '2014-12-31', 'Administrator', '/userfiles/images/2(1).png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_article_lang` (
  `article_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `article_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`article_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_article_lang`
--

INSERT INTO `tbl_article_lang` (`article_lang_id`, `article_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2),
(5, 3, 1),
(6, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_calendar`
--

CREATE TABLE IF NOT EXISTS `tbl_calendar` (
  `calendar_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  PRIMARY KEY  (`calendar_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_calendar`
--

INSERT INTO `tbl_calendar` (`calendar_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `time`, `date`, `title`, `detail`) VALUES
(1, 1, 0, 0, 2, 'show', '2014-06-09 15:05:41', 2, '2014-06-09 16:37:45', 2, 'วันขอบคุณปวงเทพเทวะ', '10.30-12.00', '2014-08-06', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '<p>\r\n	ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ</p>\r\n'),
(2, 1, 0, 0, 1, 'show', '2014-06-09 15:06:36', 2, '2014-06-09 16:10:06', 2, 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์', '10.30-19.00', '2014-05-07', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '<p>\r\n	ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_calendar_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_calendar_lang` (
  `calendar_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `calendar_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`calendar_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_calendar_lang`
--

INSERT INTO `tbl_calendar_lang` (`calendar_lang_id`, `calendar_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_contact` (
  `contact_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY  (`contact_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`contact_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `surname`, `tel`, `email`, `message`) VALUES
(1, 1, 0, 0, 1, 'show', '2014-06-11 18:22:13', 0, '2014-06-11 18:22:13', 0, 'somphong', 'play', '024685599', 'phong2u@hotmail.com', 'text'),
(2, 1, 0, 0, 1, 'show', '2014-06-11 18:23:28', 0, '2014-06-11 18:23:28', 0, 'somphong', 'play', '024685599', 'phong2u@hotmail.com', 'text'),
(3, 1, 0, 0, 1, 'show', '2014-06-11 18:24:44', 0, '2014-06-11 18:24:44', 0, 'somphong', 'play', '024685599', 'phong2u@hotmail.com', 'text'),
(4, 1, 0, 0, 1, 'show', '2014-06-11 18:25:48', 0, '2014-06-11 18:25:48', 0, 'somphong', 'play', '024685599', 'phong2u@hotmail.com', 'text'),
(5, 1, 0, 0, 1, 'show', '2014-06-11 18:27:30', 0, '2014-06-11 18:27:30', 0, 'somphong', 'play', '024685599', 'phong2u@hotmail.com', 'text'),
(6, 1, 0, 0, 1, 'show', '2014-06-11 18:29:39', 0, '2014-06-11 18:29:39', 0, 'somphong2', 'play', '024685599', 'phong2u@hotmail.com', 'text');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contactus_about`
--

CREATE TABLE IF NOT EXISTS `tbl_contactus_about` (
  `contactus_about_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `map` text NOT NULL,
  `address` text NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  PRIMARY KEY  (`contactus_about_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_contactus_about`
--

INSERT INTO `tbl_contactus_about` (`contactus_about_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `map`, `address`, `facebook`, `twitter`) VALUES
(1, 1, 0, 0, 0, 'show', '2014-06-11 17:49:48', 2, '2014-06-11 18:31:40', 2, '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7763.307215920085!2d100.9951672359552!3d13.371801888798618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d35c023fd2b1f%3A0x61317754beb0d74c!2z4Lie4Li44LiX4LiY4Liq4Lih4Liy4LiE4Lih4LiI4Li14LmI4Liu4LiH4LmB4Lir4LmI4LiH4Lib4Lij4Liw4LmA4LiX4Lio4LmE4LiX4LiiICjguIjguLXguYjguK7guIfguYDguIHguLLguLAp!5e0!3m2!1sth!2sth!4v1402483961739" width="913" height="424" frameborder="0" style="border:0"></iframe>', ' <p>พุทธสมาคมจี่ฮงแห่งประเทศไทย จี่ฮงเกาะ</p>\r\n                <p>8/1 Moo 1 Sukprayoor Road, T. Bansuan, A. Muang, </p>\r\n                <p>Chonburi, Chon Buri, Thailand 20000</p>\r\n                <p>โทร : 038 282 867</p>', 'https://www.facebook.com/', 'https://www.twitter.com/');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contactus_about_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_contactus_about_lang` (
  `contactus_about_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `contactus_about_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`contactus_about_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_contactus_about_lang`
--

INSERT INTO `tbl_contactus_about_lang` (`contactus_about_lang_id`, `contactus_about_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_contact_lang` (
  `contact_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `contact_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`contact_lang_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery_news`
--

CREATE TABLE IF NOT EXISTS `tbl_gallery_news` (
  `gallery_news_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY  (`gallery_news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_gallery_news`
--

INSERT INTO `tbl_gallery_news` (`gallery_news_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `image`) VALUES
(1, 1, 3, 0, 3, 'show', '2014-06-10 14:49:34', 2, '2014-06-10 14:49:34', 2, 'image1', '/userfiles/images/1(1).png'),
(2, 1, 3, 0, 2, 'show', '2014-06-10 14:49:47', 2, '2014-06-10 14:49:47', 2, 'image2', '/userfiles/images/thumb.png'),
(3, 1, 3, 0, 1, 'show', '2014-06-10 14:50:17', 2, '2014-06-10 14:50:17', 2, 'image3', '/userfiles/images/1(1).png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery_news_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_gallery_news_lang` (
  `gallery_news_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `gallery_news_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`gallery_news_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_gallery_news_lang`
--

INSERT INTO `tbl_gallery_news_lang` (`gallery_news_lang_id`, `gallery_news_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2),
(5, 3, 1),
(6, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home_slide`
--

CREATE TABLE IF NOT EXISTS `tbl_home_slide` (
  `home_slide_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY  (`home_slide_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_home_slide`
--

INSERT INTO `tbl_home_slide` (`home_slide_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `image`, `link`) VALUES
(1, 1, 0, 0, 1, 'show', '2014-06-09 13:09:11', 2, '2014-06-09 13:59:43', 2, 'image1', '/userfiles/images/1.png', 'https://www.facebook.com/'),
(2, 1, 0, 0, 2, 'show', '2014-06-09 13:09:28', 2, '2014-06-09 13:09:28', 2, 'image2', '/userfiles/images/1.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home_slide_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_home_slide_lang` (
  `home_slide_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `home_slide_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`home_slide_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_home_slide_lang`
--

INSERT INTO `tbl_home_slide_lang` (`home_slide_lang_id`, `home_slide_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inside`
--

CREATE TABLE IF NOT EXISTS `tbl_inside` (
  `inside_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY  (`inside_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_inside`
--

INSERT INTO `tbl_inside` (`inside_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `image`) VALUES
(1, 1, 0, 0, 1, 'show', '2014-06-11 17:02:00', 2, '2014-06-30 14:13:15', 1, 'Chee Garden', '/userfiles/images/inside/cheegarden/CheeGarden_1.png'),
(2, 1, 0, 0, 2, 'show', '2014-06-11 17:02:15', 2, '2014-06-30 14:31:49', 1, 'พลับพลาตรีดารา', '/userfiles/images/inside/%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2/%E0%B8%9E%E0%B8%A5%E0%B8%B1%E0%B8%9A%E0%B8%9E%E0%B8%A5%E0%B8%B2%E0%B8%95%E0%B8%A3%E0%B8%B5%E0%B8%94%E0%B8%B2%E0%B8%A3%E0%B8%B2_1.png'),
(3, 1, 0, 0, 3, 'show', '2014-06-11 17:02:26', 2, '2014-07-01 15:48:46', 1, 'วิหารเง็กอ้วงไต่เทียงจุง', '/userfiles/images/inside/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B9%80%E0%B8%97%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg'),
(4, 1, 0, 0, 4, 'show', '2014-06-11 17:02:38', 2, '2014-07-01 15:51:29', 1, 'วิหารไช่แปะแชกุง', '/userfiles/images/inside/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B9%84%E0%B8%8A%E0%B9%88%E0%B9%81%E0%B8%9B%E0%B8%B0%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg'),
(5, 1, 0, 0, 5, 'show', '2014-06-11 17:02:47', 2, '2014-07-01 15:55:49', 1, 'วิหารซือจุง', '/userfiles/images/inside/%E0%B8%8B%E0%B8%B7%E0%B8%AD%E0%B8%88%E0%B8%B8%E0%B8%87/%E0%B8%8B%E0%B8%B7%E0%B8%AD%E0%B8%88%E0%B8%B8%E0%B8%87_1.jpg'),
(6, 1, 0, 0, 6, 'show', '2014-06-11 17:06:26', 2, '2014-07-01 15:58:11', 1, 'วิหารเต๋าโจ้ว', '/userfiles/images/inside/%E0%B9%80%E0%B8%95%E0%B9%8B%E0%B8%B2%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%80%E0%B8%95%E0%B9%8B%E0%B8%B2%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg'),
(7, 1, 0, 0, 7, 'show', '2014-07-01 15:59:59', 1, '2014-07-01 16:00:12', 1, 'วิหารไต่เสี่ยฮุกโจ้ว', '/userfiles/images/inside/%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7/%E0%B9%84%E0%B8%95%E0%B9%88%E0%B9%80%E0%B8%AA%E0%B8%B5%E0%B9%88%E0%B8%A2%E0%B8%AE%E0%B8%B8%E0%B8%81%E0%B9%82%E0%B8%88%E0%B9%89%E0%B8%A7_1.jpg'),
(8, 1, 0, 0, 8, 'show', '2014-07-01 16:01:46', 1, '2014-07-01 16:01:46', 1, 'วิหารน่ำปักแชกุง', '/userfiles/images/inside/%E0%B8%99%E0%B9%88%E0%B8%B3%E0%B8%9B%E0%B8%B1%E0%B8%81%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87/%E0%B8%99%E0%B9%88%E0%B8%B3%E0%B8%9B%E0%B8%B1%E0%B8%81%E0%B9%81%E0%B8%8A%E0%B8%81%E0%B8%B8%E0%B8%87_1.jpg'),
(9, 1, 0, 0, 9, 'show', '2014-07-01 16:03:10', 1, '2014-07-01 16:03:25', 1, 'วิหารพระกษิติครรภ์', '/userfiles/images/inside/%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C/%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%A9%E0%B8%B4%E0%B8%95%E0%B8%B4%E0%B8%84%E0%B8%A3%E0%B8%A3%E0%B8%A0%E0%B9%8C_1.jpg'),
(10, 1, 0, 0, 10, 'show', '2014-07-01 16:04:57', 1, '2014-07-01 16:04:57', 1, 'วิหารพระพุทธ-พุทธวิหาร', '/userfiles/images/inside/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A7%E0%B8%B4%E0%B8%AB%E0%B8%B2%E0%B8%A3_1.jpg'),
(11, 1, 0, 0, 11, 'show', '2014-07-01 16:07:10', 1, '2014-07-01 16:07:10', 1, 'วิหารพระอวโลกิเตศวรโพธิสัตว์', '/userfiles/images/inside/%E0%B8%AD%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81%E0%B8%B4%E0%B9%80%E0%B8%95%E0%B8%A8%E0%B8%A7%E0%B8%A3/%E0%B8%AD%E0%B8%A7%E0%B9%82%E0%B8%A5%E0%B8%81%E0%B8%B4%E0%B9%80%E0%B8%95%E0%B8%A8%E0%B8%A7%E0%B8%A3_1.jpg'),
(12, 1, 0, 0, 12, 'show', '2014-07-01 16:21:25', 1, '2014-07-01 16:21:25', 1, 'พุทธรังษีรัตนเจดีย์', '/userfiles/images/inside/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A3%E0%B8%B1%E0%B8%87%E0%B8%A9%E0%B8%B5/%E0%B8%9E%E0%B8%B8%E0%B8%97%E0%B8%98%E0%B8%A3%E0%B8%B1%E0%B8%87%E0%B8%A9%E0%B8%B5_1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inside_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_inside_lang` (
  `inside_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `inside_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`inside_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tbl_inside_lang`
--

INSERT INTO `tbl_inside_lang` (`inside_lang_id`, `inside_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2),
(5, 3, 1),
(6, 3, 2),
(7, 4, 1),
(8, 4, 2),
(9, 5, 1),
(10, 5, 2),
(11, 6, 1),
(12, 6, 2),
(13, 7, 1),
(14, 7, 2),
(15, 8, 1),
(16, 8, 2),
(17, 9, 1),
(18, 9, 2),
(19, 10, 1),
(20, 10, 2),
(21, 11, 1),
(22, 11, 2),
(23, 12, 1),
(24, 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `news_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY  (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`news_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `title`, `date`, `time`, `detail`, `image`) VALUES
(1, 1, 0, 0, 3, 'show', '2014-06-09 14:26:46', 2, '2014-06-10 13:59:27', 2, 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '2014-06-09', '07:41', '<p>\r\n	<img alt="" src="/userfiles/images/img.png" style="width: 359px; height: 703px; margin-left: 15px; margin-right: 15px; float: left;" /></p>\r\n<p>\r\n	8 December 2012</p>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n', '/userfiles/images/1(1).png'),
(2, 1, 0, 0, 2, 'show', '2014-06-09 14:42:23', 2, '2014-06-10 13:59:12', 2, 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '2014-06-13', '8.00', '<p>\r\n	<img alt="" src="/userfiles/images/img.png" style="width: 359px; height: 703px; margin-left: 15px; margin-right: 15px; float: left;" /></p>\r\n<p>\r\n	8 December 2012</p>\r\n<h4>\r\n	<span style="color:#b22222;">พิธีวางศิลาฤกษ์มหาเจดีย์&quot;พุทธรังษีรัตนเจดีย์&quot;</span></h4>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n<p>\r\n	เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศ ไทย เป็นประธานในพิธี โดยมีวัตถุประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณ สังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประเทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรมของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควร แก่การสักการะบูชาเพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป เมื่อวันเสาร์ที่ 8 ธันวาคม 2555 ที่ ผ่านมา นายประวิทย์ หงรัตนากร นายกพุทธสมาคมจี่ฮงแห่งประเทศไทย เป็นประธานในพิธี โดยมีวัตถุ ประสงค์ เพื่อเป้นที่ประดิษฐานพระบรมสารีริกธาต ที่สมเด็จพระญาณสังวร สกลมหาสังฆปริยณายก ทรงพระกรุณาประทานให้กับพุทธสมาคมจี่ฮงแห่งประ-เทศไทย(จี่ฮงเกาะ) โดยแบบสถาปัตยกรรม ของพระเจดีย์เป็นศิลปะแบบจีน เมื่อก่อสร้างแล้วเสร็จจะได้เป็นปูชนียสถานควรแก่การสักการะบูชา เพื่อความเป็นสิริมงคลและเจริญรุ่งเรื่องสืบไป</p>\r\n', '/userfiles/images/1(1).png'),
(3, 1, 0, 0, 1, 'show', '2014-06-10 09:53:55', 2, '2014-06-10 13:59:01', 2, 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์', 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์ เมื่อวันที่ 02 เมษายน 2557 ขอเชิญผู้มีจิตศรัทธา ร่วมบุญสร้าง พุทธรังษีรัตนเจดีย์ เพื่อประดิษฐาน พระบรมสารีริกธาตุ', '2014-06-10', '10.30-19.00', '<p>\r\n	d</p>\r\n', '/userfiles/images/thumb.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_news_lang` (
  `news_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `news_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`news_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_news_lang`
--

INSERT INTO `tbl_news_lang` (`news_lang_id`, `news_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2),
(5, 3, 1),
(6, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_link`
--

CREATE TABLE IF NOT EXISTS `tbl_other_link` (
  `other_link_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY  (`other_link_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_other_link`
--

INSERT INTO `tbl_other_link` (`other_link_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `link`) VALUES
(1, 1, 0, 0, 3, 'show', '2014-06-11 19:03:11', 2, '2014-06-11 19:03:11', 2, 'เต็กก่าซิมเตี้ยง', ''),
(2, 1, 0, 0, 2, 'show', '2014-06-11 19:03:17', 2, '2014-06-11 19:03:17', 2, 'เต็กก่าซิมเตี้ยง', ''),
(3, 1, 0, 0, 1, 'show', '2014-06-11 19:03:23', 2, '2014-06-11 19:06:12', 2, 'เต็กก่าซิมเตี้ยง', 'https://www.facebook.com/');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_other_link_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_other_link_lang` (
  `other_link_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `other_link_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`other_link_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_other_link_lang`
--

INSERT INTO `tbl_other_link_lang` (`other_link_lang_id`, `other_link_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2),
(5, 3, 1),
(6, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_content`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_content` (
  `sub_content_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `a` varchar(255) NOT NULL,
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`sub_content_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_sub_content`
--

INSERT INTO `tbl_sub_content` (`sub_content_id`, `mother_shop_id`, `parent_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `a`, `recursive_id`) VALUES
(1, 1, 2, 1, 'show', '2013-11-27 09:44:30', 1, '2013-11-27 09:44:30', 1, 'a', 0),
(2, 1, 2, 2, 'show', '2013-11-27 09:44:51', 1, '2013-11-27 09:45:07', 1, 'b', 0),
(3, 1, 2, 3, 'show', '2013-11-27 09:44:58', 1, '2013-11-27 09:45:01', 1, 'c', 0),
(4, 1, 4, 1, 'show', '2013-11-27 10:02:07', 1, '2013-11-27 10:02:07', 1, 'x', 0),
(5, 1, 4, 2, 'show', '2013-11-27 10:02:13', 1, '2013-11-27 10:02:16', 1, 'y', 0),
(6, 1, 4, 3, 'show', '2013-11-27 10:02:23', 1, '2013-11-27 10:02:23', 1, 'z', 0),
(7, 2, 8, 1, 'show', '2013-11-27 10:11:54', 1, '2013-11-27 10:11:54', 1, 'x', 0),
(8, 2, 8, 2, 'show', '2013-11-27 10:12:01', 1, '2013-11-27 10:12:01', 1, 'y', 0),
(9, 2, 8, 3, 'show', '2013-11-27 10:12:07', 1, '2013-11-27 10:12:07', 1, 'z', 0),
(10, 2, 9, 1, 'show', '2013-11-27 10:13:42', 1, '2013-11-27 10:13:42', 1, 'h', 0),
(11, 2, 9, 2, 'show', '2013-11-27 10:13:49', 1, '2013-11-27 10:13:49', 1, 'j', 0),
(12, 2, 9, 3, 'show', '2013-11-27 10:13:55', 1, '2013-11-27 10:13:55', 1, 'k', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_content_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_sub_content_lang` (
  `sub_content_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `sub_content_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`sub_content_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tbl_sub_content_lang`
--

INSERT INTO `tbl_sub_content_lang` (`sub_content_lang_id`, `sub_content_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 4, 2),
(6, 5, 1),
(7, 5, 2),
(8, 6, 1),
(9, 6, 2),
(10, 7, 1),
(11, 7, 2),
(12, 8, 1),
(13, 8, 2),
(14, 9, 1),
(15, 9, 2),
(16, 10, 1),
(17, 10, 2),
(18, 11, 1),
(19, 11, 2),
(20, 12, 1),
(21, 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vdo`
--

CREATE TABLE IF NOT EXISTS `tbl_vdo` (
  `vdo_id` bigint(20) unsigned NOT NULL auto_increment,
  `mother_shop_id` int(10) unsigned NOT NULL default '1',
  `parent_id` bigint(20) unsigned NOT NULL default '0',
  `recursive_id` bigint(20) unsigned NOT NULL default '0',
  `sort_priority` bigint(20) unsigned NOT NULL,
  `enable_status` enum('show','hide','delete') NOT NULL default 'show',
  `create_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `update_by` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `embed` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY  (`vdo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_vdo`
--

INSERT INTO `tbl_vdo` (`vdo_id`, `mother_shop_id`, `parent_id`, `recursive_id`, `sort_priority`, `enable_status`, `create_date`, `create_by`, `update_date`, `update_by`, `name`, `embed`, `date`) VALUES
(1, 1, 0, 0, 0, 'show', '2014-06-10 14:04:30', 2, '2014-06-10 14:28:09', 2, 'วันขอบคุณปวงเทพเทวะ', 'kD4htT_qDmE', '2014-06-10'),
(2, 1, 0, 0, 1, 'show', '2014-06-10 14:05:28', 2, '2014-06-10 14:28:21', 2, 'ความคืบหน้าการก่อสร้างพุทธรังษีรัตนเจดีย์', 'kD4htT_qDmE', '2014-06-23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vdo_lang`
--

CREATE TABLE IF NOT EXISTS `tbl_vdo_lang` (
  `vdo_lang_id` bigint(20) unsigned NOT NULL auto_increment,
  `vdo_id` bigint(20) unsigned NOT NULL,
  `lang_id` bigint(20) unsigned NOT NULL default '0',
  PRIMARY KEY  (`vdo_lang_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_vdo_lang`
--

INSERT INTO `tbl_vdo_lang` (`vdo_lang_id`, `vdo_id`, `lang_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
         