<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class inside extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'inside';
        $dataContent['main_nav'] = 'inside';

        $this->db->select('*');
        $this->db->from('tbl_inside');
        $this->db->where('tbl_inside.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query'] = $this->db->get();

        $data['content'] = $this->load->view('inside', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

}