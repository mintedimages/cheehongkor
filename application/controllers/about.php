<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class about extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'about';
        $dataContent['main_nav'] = 'about';

        $this->db->select('*');
        $this->db->from('tbl_about');
        $this->db->where('tbl_about.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query'] = $this->db->get();
        
        
        $this->db->select('*');
        $this->db->from('tbl_about_list');
        $this->db->where('tbl_about_list.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query_list'] = $this->db->get();

        $data['content'] = $this->load->view('about', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

}