<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class article extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'article';
        $dataContent['main_nav'] = 'article';

        $this->db->select('*');
        $this->db->from('tbl_article');
        $this->db->where('tbl_article.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query'] = $this->db->get();

        $data['content'] = $this->load->view('article', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

    public function article_detail($id=0) {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'article';
        $dataContent['main_nav'] = 'article';

        $this->db->select('*');
        $this->db->from('tbl_article');
        $this->db->where('tbl_article.article_id', $id);
        $dataContent['query'] = $this->db->get();

        $data['content'] = $this->load->view('article_detail', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

}