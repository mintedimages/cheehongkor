<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class news extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'news';
        $dataContent['main_nav'] = 'news';

        $this->db->select('*');
        $this->db->from('tbl_news');
//        $this->db->join('tbl_gallery_news', 'tbl_gallery_news.parent_id = tbl_news.news_id');
//        $this->db->where('tbl_gallery_news.parent_id','tbl_news.news_id' );
        $this->db->where('tbl_news.enable_status', 'show');
        $this->db->limit(1, 0);
        $this->db->order_by('tbl_news.sort_priority');
        $dataContent['query_news_top'] = $this->db->get();

        $this->db->select('*');
        $this->db->from('tbl_news');
        $this->db->where('tbl_news.enable_status', 'show');
        $this->db->limit(2, 1);
        $this->db->order_by('sort_priority');
        $dataContent['query'] = $this->db->get();


        $this->db->select('*');
        $this->db->from('tbl_vdo');
        $this->db->where('tbl_vdo.enable_status', 'show');
        $this->db->limit(1, 0);
        $this->db->order_by('sort_priority');
        $dataContent['query_vdo'] = $this->db->get();

        $data['content'] = $this->load->view('news', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

    public function news_detail($id = 0) {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'news';
        $dataContent['main_nav'] = 'news';

        $this->db->select('*');
        $this->db->from('tbl_news');
        $this->db->where('tbl_news.news_id', $id);
        $dataContent['query'] = $this->db->get();

        $data['content'] = $this->load->view('news_detail', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

    public function event_detail($id = 0) {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'news';
        $dataContent['main_nav'] = 'news';

        $this->db->select('*');
        $this->db->from('tbl_calendar');
        $this->db->where('tbl_calendar.calendar_id', $id);
        $dataContent['query'] = $this->db->get();

        $data['content'] = $this->load->view('event_detail', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

    public function vdo() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'news';
        $dataContent['main_nav'] = 'news';

        $this->db->select('*');
        $this->db->from('tbl_vdo');
        $this->db->where('tbl_vdo.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query_vdo'] = $this->db->get();

        $data['content'] = $this->load->view('newsall', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

    public function newsall() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'news';
        $dataContent['main_nav'] = 'news';

        $this->db->select('*');
        $this->db->from('tbl_news');
        $this->db->where('tbl_news.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query'] = $this->db->get();

        $this->db->select('*');
        $this->db->from('tbl_vdo');
        $this->db->where('tbl_vdo.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query_vdo'] = $this->db->get();

        $data['content'] = $this->load->view('newsall', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

}