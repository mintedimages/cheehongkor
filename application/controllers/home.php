<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'home';
        $dataContent['main_nav'] = 'home';

        $this->db->select('*');
        $this->db->from('tbl_home_slide');
        $this->db->where('tbl_home_slide.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query_slide'] = $this->db->get();     
        
        
        $this->db->select('*');
        $this->db->from('tbl_news');
        $this->db->where('tbl_news.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query_news'] = $this->db->get();  
        
        $this->db->select('*');
        $this->db->from('tbl_calendar');
        $this->db->where('tbl_calendar.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query_event'] = $this->db->get();   
        
        $data['content'] = $this->load->view('home', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

}