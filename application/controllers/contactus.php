<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class contactus extends CI_Controller {

    public function index() {
        $data = array();
        $dataContent = array();
        $dataContent['page'] = 'contactus';
        $dataContent['main_nav'] = 'contactus';

        $this->db->select('*');
        $this->db->from('tbl_contactus_about');
        $this->db->where('tbl_contactus_about.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $dataContent['query'] = $this->db->get();

        $data['content'] = $this->load->view('contactus', $dataContent, true);
        $this->load->view('masterpage', $data);
    }

    public function register() {
        if (!$this->SpamModel->check_spam($_POST['name'])
                && !$this->SpamModel->check_spam($_POST['surname'])
                && !$this->SpamModel->check_spam($_POST['tel'])
                && !$this->SpamModel->check_spam($_POST['email'])
                && !$this->SpamModel->check_spam($_POST['message'])) {
            $this->db->set('name', $_POST['name']);
            $this->db->set('surname', $_POST['surname']);
            $this->db->set('tel', $_POST['tel']);
            $this->db->set('email', $_POST['email']);
            $this->db->set('message', $_POST['message']);
            $this->db->set('enable_status', 'show');
            $this->db->set('sort_priority', '1');
            $this->db->set('create_date', 'NOW()', FALSE);
            $this->db->set('create_by', '0');
            $this->db->set('update_date', 'NOW()', FALSE);
            $this->db->set('update_by', '0');
            $this->db->set('mother_shop_id', '1');
            $this->db->insert('tbl_contact');

            $mail_subject = 'Register from ' . $this->ConfigModel->getWebsiteName() . ' : (' . $_POST['email'] . ')';
            $mail_message = $this->load->view('email', $_POST, TRUE);
            $this->SendMailModel->smtpmail($_POST['email'], $mail_subject, $mail_message);

//            $mail_message = $this->load->view('sales_email', $_POST, TRUE);
//            $this->SendMailModel->smtpSalesMail($mail_subject, $mail_message);

            ob_start();
            redirect('contactus/index');
        } else {
            redirect('contactus/index');
        }
    }

}