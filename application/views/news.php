<div id="news-content">
    <div class="container">
        <h2>ข่าวล่าสุด</h2>
        <div class="row">
            <div class="col-xs-12"> 
                <?php
                foreach ($query_news_top->result() as $row) {
                    ?>
                    <table style=" width:970px">
                        <tr>
                            <td  style=" width:516px">
                                <div style=" width: 516px">
                                    <ul class="bxslider_news">
                                        <?php
                                        foreach ($this->util_model->gallery_news($row->news_id)->result() as $row2) {
                                            ?>
                                            <li><a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"> <img alt="" class="img-news" src="<?php echo $row2->image; ?>"></a></li>

                                        <?php } ?>
                                    </ul>
                                    <div class="list-thumb">
                                        <div id="bx-pager2">
                                            <?php
                                            $i = -1;
                                            foreach ($this->util_model->gallery_news($row->news_id)->result() as $row3) {
                                                $i++;
                                                ?>
                                                <a data-slide-index="<?php echo $i; ?>" href=""><img alt="" class="" width="51" height="51" src="<?php echo $row3->image; ?>"></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="date"><?php echo date("d M Y", strtotime($row->date)); ?> เวลา <?php echo $row->time; ?> </p>

                                <p class="titile">  <a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"> <?php echo $row->name; ?> </a></p> 
                                <p class="detail-news">
                                    <?php echo $row->title; ?>


                                </p>
                                <p class="social">

    <!--<div class="fb-share-button" data-href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>" data-type="icon"></div>-->

                                    <a  target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"><img class="" src="<?php echo base_url(); ?>assets/images/news/fb.png"/></a>

                                    <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"><img class="" src="<?php echo base_url(); ?>assets/images/news/tw.png"/></a>

    <!--<a href=""><img class="" src="<?php echo base_url(); ?>assets/images/news/fb.png"/></a>-->
    <!--<a href=""><img class="" src="<?php echo base_url(); ?>assets/images/news/tw.png"/></a>-->
                                </p>  
                            </td>
                        </tr>
                    </table> 
                <?php } ?>
            </div>

        </div>
        <div class="row">
            <div class="col-xs-6"> 
                <table style=" width:100%">
                    <tr style="background-color: #801309;color:#FFF">
                        <td style=" width: 50%; padding: 10px;  ">
                            <p >ข่าวที่เกี่ยวข้อง</p>
                        </td>  
                        <td style=" width: 50%; padding: 10px;  ">
                            <p class="more" ><a href="<?= base_url(); ?>index.php/news/newsall">ดูทั้งหมด</a></p>
                        </td> 
                    </tr>
                </table>
                <div class="row">
                    <?php
                    foreach ($query->result() as $row) {
                        ?>
                        <div class="col-xs-6 ">
                            <div class="list">
                                <a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>">    <img alt="" class="img-news" src="<?php echo $row->image; ?>"></a>

                                <p class="date-list"><?php echo date("d M Y", strtotime($row->date)); ?> เวลา <?php echo $row->time; ?> </p>
                                <h4><a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"><?php echo $row->name; ?></a></h4>
                                <p><?php echo $row->title; ?></p>
                                <p class="more-list"><a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"> อ่านต่อ ..</a></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div>
            <div class="col-xs-6 "> 
                <table style=" width:100%">
                    <tr style="background-color: #801309;color:#FFF">
                        <td style=" width: 50%; padding: 10px;  ">
                            <p >วีดีโอ</p>
                        </td>  
                        <td style=" width: 50%; padding: 10px;  ">
                            <p class="more" ><a href="<?= base_url(); ?>index.php/news/newsall">ดูทั้งหมด</a></p>


                        </td> 
                    </tr>
                </table>
                <?php
                foreach ($query_vdo->result() as $row) {
                    ?>
                    <table class="" style=" width:100%">
                        <tr>
                            <td style=" padding-top: 10px; text-align: center">


                                <iframe width="445" height="270" src="//www.youtube.com/embed/<?php echo $row->embed; ?>" frameborder="0" allowfullscreen></iframe>
                            </td>  
                        <tr>
                            <td class="vdo-detail">
                                <h4><?php echo $row->name; ?></h4>
                                <p>อัปโหลดเมื่อ <?php echo date("d M Y", strtotime($row->date)); ?></p>
                            </td>
                        </tr>

                        </tr>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
</div>