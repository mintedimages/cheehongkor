<div id="contactus-content">
    <div class="container">
         <?php
                foreach ($query->result() as $row) {
                    ?>
        <div class="row">
            <div class="col-xs-12 list">
               <?php echo $row->map; ?>
            </div> 
        </div>

        <div class="row">
            <div class="col-xs-12 list">
                <h2>ติดต่อเรา</h2>
                <?php echo $row->address; ?>
                <p>  <a href="<?php echo $row->facebook; ?>"/><img class="" src="<?php echo base_url(); ?>assets/images/contactus/fb.png"/></a>
                    <a href="<?php echo $row->twitter; ?>"/><img class="" src="<?php echo base_url(); ?>assets/images/contactus/tw.png"/></a>
                    <!--<a href=""><img class="" src="<?php echo base_url(); ?>assets/images/contactus/www.png"/></a>-->
                </p>
            </div> 
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <h2>แบบฟอร์มติดต่อ</h2>

                <div class="contact-form">
                           <form action="<?php echo base_url(); ?>index.php/contactus/register" method="post" accept-charset="utf-8" id="contact-form"> 
                        <div class="row">
                            <div class="col-xs-7">
                                <table>
                                    <tr>
                                        <td width="100">
                                            <b>ชื่อ</b>
                                        </td>
                                        <td>
                                            <input type="text" name="name" class="validate[required]" id="name">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>นามสกุล</b>
                                        </td>
                                        <td>
                                            <input type="text" name="surname" class="validate[required]" id="surname">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>เบอร์โทรติดต่อ</b>
                                        </td>
                                        <td>
                                            <input type="text" name="tel" class="validate[required,custom[phone],minSize[9],maxSize[11]]" id="tel">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>อีเมล</b>
                                        </td>
                                        <td>
                                            <input type="text" name="email" class="validate[required,custom[email]]" id="email">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-xs-5">
                                <table>
                                    <tr>
                                        <td><p>ข้อความ :</p>
                                            <textarea name="message" id="message" rows="3" class="form-control"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="text-right"> <input type="submit" class="button" value="ส่งข้อความ" name="submit"></p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>