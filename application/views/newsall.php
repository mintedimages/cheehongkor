<div id="newsall-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <table style=" width:100%">
                    <tr style="background-color: #801309;color:#FFF">
                        <td>
                            <h4>ข่าวที่เกี่ยวข้อง</h4>
                        </td>  

                    </tr>
                </table>
                <?php
                foreach ($query->result() as $row) {
                    ?>
                    <table class="list">
                        <tbody>

                            <tr>
                                <td width="35%">
                                    <a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>">    <img alt="" class="img-news" src="<?php echo $row->image; ?>"></a>
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <b>  <a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"> <?php echo $row->name; ?></a></b>
                                    <p><?php echo date("d M Y", strtotime($row->date)); ?></p>
                                    <p> <?php echo $row->title; ?></p>
                                    <p class="more"><a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"> อ่านต่อ ..</a></p>
                                </td>

                            </tr>


                        </tbody>
                    </table>
                    <p class="line-list"></p>
                <?php } ?>
            </div>
            <div class="col-xs-6">
                <table style=" width:100%">
                    <tr style="background-color: #801309;color:#FFF">
                        <td>
                            <h4>วีดีโอ</h4>
                        </td>  

                    </tr>
                </table>

                <?php
                foreach ($query_vdo->result() as $row) {
                    ?>
                    <div class="list-vdo">
                        <table >
                            <tbody>

                                <tr>
                                    <td>
                                        <iframe width="192" height="117" src="//www.youtube.com/embed/<?php echo $row->embed; ?>" frameborder="0" allowfullscreen></iframe>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <b><?php echo $row->name; ?></b>
                                        <!--<p>Wednesday, January 15, 2014</p>-->
                                        <p>อัปโหลดเมื่อ : <?php echo date("d M Y", strtotime($row->date)); ?></p>
                                    </td>

                                </tr>


                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>