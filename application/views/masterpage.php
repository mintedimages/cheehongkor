<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Note there is no responsive meta tag here -->

        <link rel="shortcut icon" href="">

        <title>พุทธสมาคมจี่ฮงแห่งประเทศไทย</title>

        <!-- Bootstrap core CSS -->
        <link href="<?= base_url(); ?>assets/non_responsive/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?= base_url(); ?>assets/non_responsive/non-responsive.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="<?= base_url(); ?>assets/non_responsive/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="<?= base_url(); ?>assets/non_responsive/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="<?= base_url(); ?>assets/non_responsive/respond.min.js"></script>
        <![endif]-->

        <script src="<?= base_url(); ?>assets/non_responsive/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/non_responsive/bootstrap.min.js"></script>

        <script src="<?= base_url(); ?>assets/jquery.bxslider/jquery.bxslider.js"></script>

        <link rel="stylesheet" href="<?= base_url(); ?>assets/jquery.bxslider/jquery.bxslider.css" rel="stylesheet" media="screen" />
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" media="screen" />

        <script src="<?= base_url(); ?>assets/js/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?= base_url(); ?>assets/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>

        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/validationEngine.jquery.css" />


    <div id="fb-root"></div>
    <script type="text/javascript">
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        
        
        
    </script>


    <script type="text/javascript">
        $(document).ready(function() {
            
            $("#contact-form").validationEngine();
            
            $('.bxslider').bxSlider({
                pager:true,
                minSlides: 1,
                maxSlides: 1,
                //                    slideWidth: 970,
                slideMargin: 5
            });
                
            $('.bxslider_news').bxSlider({
                pagerCustom: '#bx-pager2'
            });
                
        });
    </script>
</head>

<body>
    <div class="container">
        <header>
            <div class="row">
                <p align="center"><img class="" src="<?php echo base_url(); ?>assets/images/logo.png"/></p>
                <!--
                <div class="col-xs-2"><img class="" src="<?php echo base_url(); ?>assets/images/logo1.png"/></div>
                <div class="col-xs-2"><img class="" src="<?php echo base_url(); ?>assets/images/logo2.png"/></div>
                <div class="col-xs-8"><img class="" src="<?php echo base_url(); ?>assets/images/text.png"/></div>
                -->
            </div>

            <?php echo $this->load->view('navigate'); ?>
            <a href="https://www.facebook.com/pages/Chee-Hong-Buddhist-Association-of-Thailand-%E0%B8%88%E0%B8%B5%E0%B9%88%E0%B8%AE%E0%B8%87%E0%B9%80%E0%B8%81%E0%B8%B2%E0%B8%B0-%E7%B4%AB%E5%B3%B0%E9%96%A3-Chee-Hong-Kor/181735004046"><img class="" src="<?php echo base_url(); ?>assets/images/fb.png"/></a>
        </header>


    </div>

    <div class="row" style=" margin: 0px">
        <div class=" line">

        </div>
    </div>
    <?php echo $content; ?>



</div> <!-- /container -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-7">
                <h4>องค์กรอื่นๆของ เต๊กก่า ประเทศไทย</h4> 
                <ul>
                    <?php
                    foreach ($this->util_model->other()->result() as $row) {
                        ?>
                        <li><a href="<?php   echo $row->link;?>"><?php   echo $row->name;?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-xs-5 right">
                <h4>ติดต่อสมาคม</h4>
                <?php
                foreach ($this->util_model->cotact_footer()->result() as $row) {
                    echo $row->address;
                }
                ?>


            </div>
        </div>
</footer>

<div class="row footer-bottom"  style=" margin: 0px">
    <div class="container">


        <div class="col-xs-12">
            <p>© 2014 Chee Hong Buddhist Association of Thailand . All Rights Reserved.</p>
        </div>
    </div>

</div>

</body>
</html>
