<div id="home-contant">
    <div id="slider">
        <div class="container">
            <ul class="bxslider">
                <?php
                foreach ($query_slide->result() as $row) {
                    ?>
                    <li><a href="<?php echo $row->link; ?>"><img alt="<?php echo $row->name; ?>" src="<?php echo $row->image; ?>"/></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container content">
        <div class="row">
            <div class="col-xs-7 list">
                <h3>ข่าวสารและกิจกรรม</h3>
                <?php
                foreach ($query_news->result() as $row) {
                    ?>
                    <div class="list-news"> 

                        <div class="row">
                            <div class="col-xs-6 ">
                                <a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>">    <img alt="" class="img-news" src="<?php echo $row->image; ?>"></a>
                            </div>
                            <div class="col-xs-6 ">

                                <b><a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>"><?php echo $row->name; ?></a></b>
                                <p>
                                    <?php echo $row->title; ?>
                                    <strong><a href="<?php echo base_url(); ?>index.php/news/news_detail/<?php echo $row->news_id; ?>">อ่านต่อ..</a></strong>
                                </p>
                            </div>
                        </div>

                    </div>
                <?php } ?>
                <p class="text-right"><a href="<?php echo base_url(); ?>index.php/news/newsall"><input type="submit" class="button" value="ดูข่าวทั้งหมด" name="submit"></a>&nbsp;&nbsp;&nbsp;</p>

            </div>
            <div class="col-xs-5">
                <h3><img class="" src="<?php echo base_url(); ?>assets/images/home/calen.png"/> ปฏิทินกิจกรรม</h3>

                <div class="row">
                    <div class="col-xs-12  date-event ">
                        <?php
                        foreach ($query_event->result() as $row2) {
                            ?>
                            <table>
                                <tr>
                                    <td  class="date" >
                                        <p class="date-num"><?php  echo $d = substr($row2->date,8, 2);?></p>
                                        <p class="date-mouth"> <?php
                                             $m = substr($row2->date,5, 2) + 1;
                                            echo date("F", strtotime("00-$m-00"));
                                            ?></p>
                                        <p class="date-year"><?php echo   $y = substr($row2->date,0, 4); ?></p>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <b><?php echo $row2->name;?></b>
                                        <p>เวลา <?php echo $row2->time; ?> น.</p>
                                        <p>
                                           <?php echo $row2->title;?>
                                        </p>
<!--                                        <p>10:30a.m.-12:00p.m.</p>-->
                                        <strong> <a href="<?php echo base_url(); ?>index.php/news/event_detail/<?php echo $row2->calendar_id; ?>">อ่านต่อ...</a></strong>
                                    </td>
                                </tr>
                            </table>
                            <p class="line-date"></p>
                        <?php } ?>
                    </div>

                </div>
                <img class="" src="<?php echo base_url(); ?>assets/images/home/fb.png"/> 
                
                
                <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FChee-Hong-Buddhist-Association-of-Thailand-%25E0%25B8%2588%25E0%25B8%25B5%25E0%25B9%2588%25E0%25B8%25AE%25E0%25B8%2587%25E0%25B9%2580%25E0%25B8%2581%25E0%25B8%25B2%25E0%25B8%25B0-%25E7%25B4%25AB%25E5%25B3%25B0%25E9%2596%25A3-Chee-Hong-Kor%2F181735004046%3Ffref%3Dts&amp;width=359&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:359px; height:258px;" allowTransparency="true"></iframe>
            </div>
        </div>

    </div>
</div>