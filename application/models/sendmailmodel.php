<?php

class SendMailModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function smtpmail($email, $subject, $body) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = "utf-8";  // ในส่วนนี้ ถ้าระบบเราใช้ tis-620 หรือ windows-874 สามารถแก้ไขเปลี่ยนได้                         
        $mail->Host = $this->ConfigModel->getSMTP(); //  mail server ของเรา
        $mail->SMTPAuth = true;     //  เลือกการใช้งานส่งเมล์ แบบ SMTP
        $mail->Username = $this->ConfigModel->getSender();   //  account e-mail ของเราที่ต้องการจะส่ง
        $mail->Password = $this->ConfigModel->getSenderPassword();  //  รหัสผ่าน e-mail ของเราที่ต้องการจะส่ง

        $mail->From = $this->ConfigModel->getSender();  //  account e-mail ของเราที่ใช้ในการส่งอีเมล
        $mail->FromName = $this->ConfigModel->getSender(); //  ชื่อผู้ส่งที่แสดง เมื่อผู้รับได้รับเมล์ของเรา
        $mail->AddAddress($email);            // Email ปลายทางที่เราต้องการส่ง(ไม่ต้องแก้ไข)
        $mail->IsHTML(true);                  // ถ้า E-mail นี้ มีข้อความในการส่งเป็น tag html ต้องแก้ไข เป็น true
        $mail->Subject = $subject;        // หัวข้อที่จะส่ง(ไม่ต้องแก้ไข)
        $mail->Body = $body;                   // ข้อความ ที่จะส่ง(ไม่ต้องแก้ไข)
        $result = $mail->send();
        return $result;
    }

    function smtpSalesMail($subject, $body) {
        require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = "utf-8";  // ในส่วนนี้ ถ้าระบบเราใช้ tis-620 หรือ windows-874 สามารถแก้ไขเปลี่ยนได้                         
        $mail->Host = $this->ConfigModel->getSMTP(); //  mail server ของเรา
        $mail->SMTPAuth = true;     //  เลือกการใช้งานส่งเมล์ แบบ SMTP
        $mail->Username = $this->ConfigModel->getSender();   //  account e-mail ของเราที่ต้องการจะส่ง
        $mail->Password = $this->ConfigModel->getSenderPassword();  //  รหัสผ่าน e-mail ของเราที่ต้องการจะส่ง

        $mail->From = $this->ConfigModel->getSender();  //  account e-mail ของเราที่ใช้ในการส่งอีเมล
        $mail->FromName = $this->ConfigModel->getSender(); //  ชื่อผู้ส่งที่แสดง เมื่อผู้รับได้รับเมล์ของเรา
        $receiverArr = $this->ConfigModel->getReceiver();
        foreach ($receiverArr as $value) {
            $mail->AddAddress($value, $value);            // Email ปลายทางที่เราต้องการส่ง(ไม่ต้องแก้ไข)
        }
        $receiverCCArr = $this->ConfigModel->getReceiverCC();
        foreach ($receiverCCArr as $value) {
            $mail->AddCC($value, $value);
        }
        $receiverBCCArr = $this->ConfigModel->getReceiverBCC();
        foreach ($receiverBCCArr as $value) {
            $mail->AddBCC($value, $value);
        }

        $mail->IsHTML(true);                  // ถ้า E-mail นี้ มีข้อความในการส่งเป็น tag html ต้องแก้ไข เป็น true
        $mail->Subject = $subject;        // หัวข้อที่จะส่ง(ไม่ต้องแก้ไข)
        $mail->Body = $body;                   // ข้อความ ที่จะส่ง(ไม่ต้องแก้ไข)
        $result = $mail->send();
        return $result;
    }

}

?>
