<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of languagemodel
 *
 * @author tomozard
 */
class LanguageModel extends CI_Model {

    private $default_lang = 'th';

    function __construct() {
        parent::__construct();
    }

    function get_language() {
        if ($this->session->userdata('lang')) {
            return $this->session->userdata('lang');
        } else {
            $this->session->set_userdata('lang', $this->default_lang);
            return $this->session->userdata('lang');
        }
    }

    function getLanguageId() {
        $this->db->where('lang_code',$this->get_language());
        $query = $this->db->get('mother_lang');
        $row = $query->row();
        return $row->lang_id;
    }

}

?>
