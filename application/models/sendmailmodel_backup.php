<?php

class SendMailModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function send($mail_to, $mail_subject, $mail_message) {
        $mail_from = "sales@theportraitrama4.com";
        $SMTP = "mail.theportraitrama4.com";
        $mail_message = nl2br($mail_message);
        $Headers = "MIME-Version: 1.0\r\n";
        $Headers .= "Content-type: text/html; charset=UTF-8\r\n";
        $Headers .= "From: " . $mail_from . " <" . $mail_from . ">\r\n";
        $Headers .= "Reply-to: " . $mail_from . " <" . $mail_from . ">\r\n";
        $Headers .= "Cc: Monchai.Orawongpaisan@colliers.com\r\n";
        $Headers .= "Cc: Thiprachaya.Piyapachjiragul@colliers.com\r\n";
        $Headers .= "Cc: Suthida.Sirawong@colliers.com\r\n";
        $Headers .= "Cc: sc.nook@gmail.com\r\n";
        $Headers .= "Cc: Napatr.tienchutima@colliers.com\r\n";
        $Headers .= "Cc: sales@theportraitrama4.com\r\n";
        $Headers .= "Cc: mint@mintedimages.com\r\n";
        $Headers .= "Cc: achiraya@mintedimages.com\r\n";
        $Headers .= "Cc: narathip@mintedimages.com\r\n";
        $Headers .= "X-Priority: 3\r\n";
        $Headers .= "X-Mailer: PHP mailer\r\n";
        ini_set("SMTP", $SMTP);
        ini_set("SMTP_PORT", 25);
        ini_set("sendmail_from", $mail_from);
        return mail($mail_to, $mail_subject, $mail_message, $Headers, $mail_from);
    }

}

?>