<?php

class util_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getNow() {
        $timestamp = now();
        $timezone = 'UM1';
        $daylight_saving = TRUE;
        $timeTh = gmt_to_local($timestamp, $timezone, $daylight_saving);
        return unix_to_human($timeTh, TRUE, 'eu');
    }

    function thisDate($date) {
//        return substr($date, 8, 2);
        return date("d", strtotime($date));
    }

    function thisMonth($date) {
//        $month = substr($date, 5, 2);
//        $mons = array( 01 => "Jan",02 => "Feb", 03 => "Mar", 04 => "Apr", 05 => "May", 06 => "Jun", 07 => "Jul", 08 => "Aug", 09 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");
//
//        $month_name = $mons[$month];
//
//        return $month_name;

        return $strMonth = date("M", strtotime($date));

//        $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
//     return  $strMonthThai=$strMonthCut[$strMonth];
    }

    function gallery_news($id) {
        $this->db->select('*');
        $this->db->from('tbl_gallery_news');
        $this->db->where('tbl_gallery_news.parent_id', $id);
        $this->db->where('tbl_gallery_news.enable_status', 'show');
        $this->db->order_by('tbl_gallery_news.sort_priority');
        return $this->db->get();
    }

    function cotact_footer() {
        $this->db->select('*');
        $this->db->from('tbl_contactus_about');
        $this->db->where('tbl_contactus_about.enable_status', 'show');
        $this->db->order_by('sort_priority');
        return $this->db->get();
    }

    function other() {
        $this->db->select('*');
        $this->db->from('tbl_other_link');
        $this->db->where('tbl_other_link.enable_status', 'show');
        $this->db->order_by('sort_priority');
        return $this->db->get();
    }

    function thisYear($date) {
//        return substr($date, 0, 4);
        return date("Y", strtotime($date));
    }

    function LinkProject() {
        $this->db->select('*');
        $this->db->from('tbl_footer_project');
        $this->db->where('tbl_footer_project.enable_status', 'show');
        $this->db->order_by('sort_priority');
        return $this->db->get();
    }

    function LinkSocialFacebook() {
        $this->db->select('tbl_social.link_facebook');
        $this->db->from('tbl_social');
        $this->db->where('tbl_social.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $query = $this->db->get();
        $row = $query->row();
        return $row->link_facebook;
    }

    function LinkSocialYoutube() {
        $this->db->select('tbl_social.link_youtube');
        $this->db->from('tbl_social');
        $this->db->where('tbl_social.enable_status', 'show');
        $this->db->order_by('sort_priority');
        $query = $this->db->get();
        $row = $query->row();
        return $row->link_youtube;
    }

}

?>
